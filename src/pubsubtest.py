# -*- coding: utf-8 -*-


from pubsub import pub


def function(): pass

# ------------ create a listener ------------------

def listener1( arg1, arg2=None ):
    """
    function that will subscribe to a topic
    """
    print('Function listener1 received:')
    print('  arg1 =', arg1)
    print('  arg2 =', arg2)



class Foo:
    def method(self): pass

    @staticmethod
    def staticMeth(): pass

    @classmethod
    def classMeth(cls): pass

    def __call__(self): pass

foo = Foo()

def ex_basic():
    pub.subscribe( callable, 'root-topic-1')
    pub.subscribe( callable, 'root-topic-1.sub-topic-2')
    pub.subscribe( callable, 'root-topic-1.sub-topic-2.sub-sub-topic-3')



    print(f"callable {callable}")




    # ------------ register listener ------------------

    pub.subscribe( listener1, 'rootTopic')

    # ---------------- send a message ------------------

    print('Publish something via pubsub')
    anObj = dict(a=456, b='abc')
    pub.sendMessage('rootTopic', arg1 = 123, arg2 = anObj)

#ex_basic()

# ------------------ helper

class CallableObject:
    """
    this allows us to send a complicated payload
    """
    def __init__( self ):
        pass

    def get_object(self):
        """
        this is the function to publish, subscriber get everything
        else from the object.
        """
        return self

    def action( self ):
        print( f"action{ 4+4 }")

    def __call__(self): pass



# ==========================================================
def ex_with_callable_object():
    print("""
    ================ ex_with_callable_object():   ===============

    """)

    def a_subscriber( arg1, arg2 ):
        print
        print( 'a_subscriber received:')
        print( f"  arg1 the callable =  {arg1} ")
        print( f"  arg2 =  {arg2} ")
        if arg2  == "CallableObject":
            a_callable_object   = arg1( )
            a_callable_object.action()

    pub.subscribe( a_subscriber, 'pubCallableObject')


    print('Publish CallableObject via pubsub')

    a_callable_object   = CallableObject()

    # note there is a heirarchy to the topic, and can be more thant 2 args ....
    pub.sendMessage( 'pubCallableObject', arg1 = a_callable_object.get_object, arg2 = "CallableObject"    )



ex_with_callable_object()








