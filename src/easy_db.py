  # -*- coding: utf-8 -*-


"""
main file for easy_db
SQLAlchemy - The Database Toolkit for Python
    *>url  https://www.sqlalchemy.org/


"""

import os
import logging
import sys
#import traceback
import importlib
from   subprocess import Popen   #, PIPE  #
import time
import datetime
import webbrowser
#import tkinter
from   tkinter import messagebox


#----- local imports
import parameters
import gui
from   app_global import AppGlobal
import db_objects
import file_writers
import file_readers

# ------------------------------
def   print_uni( a_string_ish ):
    """
    print even if unicode char messes up conversion
    maybe doing as a call is over kill
    """
    print(  a_string_ish.encode( 'ascii', 'ignore') )

# ----------------------------------------------
def add_data_path(  file_name ):
    """
    complete short path names by adding data_dir part  ... remove instance funct of same name
    """
    return os.path.join( AppGlobal.data_dir, file_name )

# ============================================
class App( object ):
    """
    this class is the "main" or controller for the whole app
    to run see end of this file
    it is the controller of an mvc app
     """
    def __init__( self,  start_gui = True ):
        """
        usual init for main app
        splash not working as desired, disabled
        splash screen which is of not help unless we sleep the init
        """
        self.app_name          = "EasyDB"
        self.app_version       = "Ver9:  2019_12_01.01"
        self.version           = self.app_version
        AppGlobal.controller   = self
        self.gui               = None
        self.start_gui         = start_gui

        self.restart( )

    # ----------------------------------
    def restart(self ):
        """
        use to restart the app without ending it
        this can be very quick
        """
        print( f"========= restart {self.app_name}=================" )
        if not( self.gui is None ):
            self.gui.root.destroy()
            importlib.reload( parameters )    # should work on python 3 but sometimes does not
        else:
            pass

        self.parameters     = parameters.Parameters(  ) # open early as may effect other parts of code
        self.config_logger()
        self.prog_info()

        AppGlobal.logger         = self.logger
        AppGlobal.logger.log( logging.DEBUG, "test logging.DEBUG from AppGlobal" )
        AppGlobal.print_debug( "test AppGlobal.print_debug"  )
        if not( self.start_gui ):
            return

        self.gui                 = gui.GUI( self )

        AppGlobal.filein_name    = self.parameters.filein_name
        AppGlobal.fileout_name   = self.parameters.fileout_name


#        self.gui.write_gui_db_file_name(  AppGlobal.gui.get_db_file_name() )
#        self.gui.write_gui_in_file_name(  AppGlobal.filein_name  )
#        self.gui.write_gui_out_file_name( AppGlobal.fileout_name )

        #!! also others above  but may be obsolete as have new elements

#        AppGlobal.gui.get_db_file_name()       = None
#        self.change_db_file_name( self.parameters.db_file_name )

        print( "------------- set db refresh tables in AppGlobal -----------" )
        #AppGlobal.gui.get_db_file_name()      = ( self.parameters.db_file_name )
        db_file_name                 =  self.parameters.db_file_name
        AppGlobal.set_db_file_name( db_file_name )

        print( "------------- end  set db -----------" )

        self.gui.write_gui_db_file_name(  AppGlobal.get_db_file_name() )
#        self.gui.write_gui_in_file_name(  AppGlobal.filein_name  )
#        self.gui.write_gui_out_file_name( AppGlobal.fileout_name )



        AppGlobal.select_file_name   = None
        self.change_select_file_name( self.parameters.select_file_name )

        self.polling_delta  = self.parameters.poll_delta_t

        self.starting_dir   = os.getcwd()    # or perhaps parse out of command line
        self.gui.root.after( self.polling_delta, self.polling )

        self.gui.root.mainloop()

    # ------------------------------------------
    def config_logger( self, ):
        """
        configure the python logger
        """
        AppGlobal.logger_id     = "App"        # or prerhaps self.__class__.__name__
        logger                  = logging.getLogger( AppGlobal.logger_id )

        logger.handlers         = []

        logger.setLevel( self.parameters.logging_level )

        # create the logging file handler
        fh = logging.FileHandler( self.parameters.pylogging_fn )

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        logger.addHandler(fh)

#        logger.info( "Done config_logger" )
        #print( "configed logger", flush = True )
        self.logger  = logger   # for access in rest of class?
#        msg     =  f"logger level in App = {self.logger.level}"
#        print( msg )

    # --------------------------------------------
    def prog_info( self,  ):

        fll         = AppGlobal.force_log_level
        logger      = self.logger

        logger.log( fll, 2*"\n" + "============================\n" )
        title       =   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        logger.log( fll, title )
        logger.log( fll, "" )

        if len( sys.argv ) == 0:
            logger.info( "no command line arg " )
        else:

            for ix_arg, i_arg in enumerate( sys.argv ):
                msg = f"command line arg + {str( ix_arg ) }  =  { i_arg })"
                logger.log( AppGlobal.force_log_level, msg )

        logger.log( fll, f"current directory {os.getcwd()}"  )

        self.parameters.running_on.log_me( logger, logger_level = AppGlobal.force_log_level,  print_flag = True )
        start_ts     = time.time()
        dt_obj       = datetime.datetime.utcfromtimestamp( start_ts )
        string_rep   = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        logger.log( fll, "Time now: " + string_rep )
        # logger_level( "Parameters say log to: " + self.parameters.pylogging_fn ) parameters and controller not available can ge fro logger_level

        return

    # ----------------------------------
    def polling( self,  ):
        """
        what is says not currently used
        """
        pass
        return

    # ---------------- functions -------------------

    # ----------------------------------------------
    def open_output_file( self,  file_name, output_format, set_last = True ):
        """
        make one based on extension as well: no use format None
        sort of a mess, assumes os has some associations not others
        """
#        ext_to_format   = {"html":"html", "py_log":3.14, 'z':7}
        if output_format is None:
            _, ext = os.path.splitext( file_name )
            if   ext == ".html":
                 output_format = "html"
            elif ext == ".py_log":
                output_format = "py_log"
            # else it will just use text

        if  output_format == "py_log":   # why not just pass the correct file name
            file_name  = self.parameters.pylogging_fn   # ignore passed file
            self.os_open_text_file( file_name   )

        elif  output_format == "html":
            webbrowser.open( file_name, new=0, autoraise=True )

        else:
            self.os_open_text_file( file_name  )   # or log

        if set_last:
            self.change_last_output_file_name( file_name )

    # ----------------------------------------------
    def os_open_logfile( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( self.parameters.pylogging_fn )

    # ----------------------------------------------
    def os_open_help( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_help_file( self.parameters.help_fn )

    # ----------------------------------------------
    def os_open_parmfile( self,  ):
        """
        used as callback from gui button
        """
        a_filename = self.starting_dir  + os.path.sep + "parameters.py"

        AppGlobal.os_open_txt_file( a_filename )

    # ----------------------------------------------
    def bcb_open_text_editor( self,    ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( None )

    # ----------------------------------------------
    def os_file_explorer( self,    ):
        """
        used as callback from gui button
        """
        AppGlobal.os_file_explorer(  )

    # ----------------------------------------------
    def bcb_make_input_generic_form( self,   ):
        """
        used as callback from gui button

        """
        a_file_name   = "input_form_generic.txt"
        a_file_writer = file_writers.InputGenericFormWriter( a_file_name )
        a_file_writer.write_file()
        AppGlobal.os_open_txt_file( a_file_name )

     # ----------------------------------------------
    def os_open_text_file( self, file_name  ):
        """
        used as callback from gui button
        get rid of me and call directly
        """
        AppGlobal.os_open_txt_file( file_name )
    # ----------------------------------------------
    def open_last_output( self, ):
        file_name         = AppGlobal.last_output_file_name
        if file_name is None or file_name == "":   # what about multiple blanks
            msg               = f"open_last_output .. cannot open blank file name"
            AppGlobal.print_debug( msg )
            return
        msg               = f"open_last_output >>{AppGlobal.last_output_file_name}<<"
        AppGlobal.print_debug( msg )
        self.os_open_text_file( AppGlobal.last_output_file_name  )

# ====================== gui events ============
    # ----------------------------------
    def change_db_file_name( self, new_db_file_name ):
        """
        a gui event, change if valid and different
        also changes data_dir
        can use at init as well
        """
        # old file name
        if new_db_file_name == AppGlobal.old_db_file_name:
            return
        # !! still need to validate
        # need to extract data dir  -- or move to input file functions below
        AppGlobal.old_db_file_name   = new_db_file_name

        # for now assume came from gui, later fix this up

        data_dir          = os.path.dirname(  new_db_file_name )

        msg               = f"old delete  change_db_file_name {new_db_file_name} {data_dir}"
        AppGlobal.print_debug( msg )

        tables            = db_objects.get_table_list( new_db_file_name )
        if tables is None:
            tables = []
            msg               = f"tables is None {new_db_file_name} "
            AppGlobal.print_debug( msg )

        if tables ==  []:
            return
        self.gui.ddw_tables.set_list( tables )
        self.gui.ddw_tables.set_index( 0 )
        #self.gui.ddw_tables.set_text( "test set_text" )

        self.gui.set_db_file_name( new_db_file_name )
#        AppGlobal.gui.get_db_file_name()   = new_db_file_name
        #y = 1/0    # fix later
        AppGlobal.data_dir       = data_dir
        msg   = f"set data_dir to {data_dir}"
        table_name               = self.gui.ddw_tables.get_text()
        self.change_table_name( table_name )

    # ----------------------------------
    def finish_set_db_file_name( self, new_db_file_name ):
        """
        a gui event, change if valid and different
        also changes data_dir
        can use at init as well
        """
        data_dir          = os.path.dirname(  new_db_file_name )

        msg               = f"old delete  change_db_file_name {new_db_file_name} {data_dir}"
        AppGlobal.print_debug( msg )

        tables            = db_objects.get_table_list( new_db_file_name )
        if tables is None:
            tables = []
            msg               = f"tables is None {new_db_file_name} "
            AppGlobal.print_debug( msg )

        if tables ==  []:
            return
        self.gui.ddw_tables.set_list( tables )
        self.gui.ddw_tables.set_index( 0 )
        #self.gui.ddw_tables.set_text( "test set_text" )

        self.gui.set_db_file_name( new_db_file_name )
#        AppGlobal.gui.get_db_file_name()   = new_db_file_name
#        y = 2/0 # fix later
        AppGlobal.data_dir       = data_dir
        table_name               = self.gui.ddw_tables.get_text()
        self.change_table_name( table_name )

    # ----------------------------------
    def change_last_output_file_name( self, new_file_name ):
        """
        a gui event, change if valid and different
        also changes data_dir
        can use at init as well
        """
        # old file name
#        if AppGlobal.last_output_file_name == new_file_name:
#            return
        # !! still need to validate
        # need to extract data dir  -- or move to input file functions below
        AppGlobal.last_output_file_name = new_file_name

        msg               = f"change_last_output_file_name {new_file_name}"
        AppGlobal.print_debug( msg )

        self.gui.last_output_file_name_label.config( text = f"Last Output:{new_file_name}" )

   # ----------------------------------
    def change_input_file_name( self, new_file_name ):
        """
        a gui event, change if valid and different
        also changes data_dir
        can use at init as well
        """
        # old file name
        if AppGlobal.filein_name == new_file_name:
            return
        # !! still need to validate
        # need to extract data dir
#        data_dir          = os.path.dirname(  new_db_file_name )
#        tables            = db_objects.get_table_list( new_db_file_name )
#        if tables is None:
#            tables = []
#
#        self.gui.ddw_tables.set_list( tables )

#        self.gui.set_db_file_name( new_db_file_name )
        AppGlobal.filein_name   = new_file_name
#        AppGlobal.data_dir       = data_dir

   # ----------------------------------
    def change_table_name( self, table_name ):
        """
        a gui event, change if valid and different
        can use at init as well
        self.controller.change_table_name( table_name )
        """
        msg     = f"change_table_name =>>{table_name}<<"
        AppGlobal.print_debug( msg )
#        if AppGlobal.table_name  == table_name:  # may not be necessary
#            return
        AppGlobal.table_name      = table_name
        AppGlobal.gui.ddw_tables.set_text( table_name )

   # ----------------------------------
    def change_select_file_name( self, new_file_name ):
        """
        a gui event, change if valid and different
        can use at init as well
        """
        msg     = f"change_select_file_name =>>{new_file_name}<<"
        AppGlobal.print_debug( msg )
#        if AppGlobal.select_file_name     == new_file_name:
#            return
        AppGlobal.select_file_name         = new_file_name
        AppGlobal.gui.set_select_file_name(  new_file_name )

    # ----------------------------------
    def setup_table_info( self, ):
        """
        store some data dict info, this may be a short term thing
        """
        self.parameters.make_table_info_room( )
        self.parameters.make_table_info_observation( )
        self.parameters.make_table_info_mush( )

    # ----------------- Buttons start with bcb buttons -----------------

        # ----------------------------------------------
    def bcb_define_db( self,  ):
        """
        define a new sql data base file
        must be in a new file ( not currently existing )
        """
        db_file_name    = AppGlobal.gui.get_db_file_name()   # !! may want to change to use a dialog
        if os.path.isfile( db_file_name ):
#            msg   =  f"File exists: {db_file_name}"
#            AppGlobal.gui.display_info_string( msg )
#            print( f"file exists: {db_file_name}" )
            msg   =  f"Error: File ({db_file_name}) already exists."
            AppGlobal.write_gui( msg )
            messagebox.showinfo( "Error", msg )
            return
        msg     = f"Creating empty database in file ({db_file_name})."
        AppGlobal.write_gui( msg )
        define_db.create_db( db_file_name )
        msg     = f"Empty database in file ({db_file_name}) done."
        AppGlobal.write_gui( msg )

    # ----- define table  ready for rename !!
    def bcb_define_table( self ):  # place holder for function not yet determined

#        msg   = "\n"*2 +"tbd_bcb_wf_1 define_table"
#        print( msg )
#        self.write_gui( msg  )
#        self.try_make_and_run_table_def_sql( self.add_data_path( self.parameters.db_file_name ),
#                                             self.add_data_path( self.parameters.filein_name ), )

        db_file_name  = AppGlobal.gui.get_db_file_name()
        in_file_name  = AppGlobal.filein_name

        msg  = f"Define Table {db_file_name} using {in_file_name}"
        AppGlobal.gui_write_start( msg )

        # define db if not exist

        try:
            table_info   = db_objects.TableInfo( )
            table_info.build_from_input_file( in_file_name,  print_out=True )
            #print( table_info.to_sql() )
            table_access  = db_objects.TableAccess( db_file_name, table_info  )
            table_access.define_table_from_table_info()
            # success  # refresh db file
            self.change_table_name( table_info.table_name )
#            temp                    = AppGlobal.gui.get_db_file_name()
#            AppGlobal.gui.get_db_file_name()  = None
#            self.change_db_file_name( temp )

            msg    = f"... new table defined: {table_info.table_name}"
            AppGlobal.write_gui( msg )
        except db_objects.DBOjectException as exception:
            print( exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg  = "Define Table"
        AppGlobal.gui_write_finish( msg )

    # -------------------------------
    def bcb_select_using_file( self, ):
        """

        """
        db_file_name         = AppGlobal.gui.get_db_file_name()
        input_file_name      = AppGlobal.select_file_name

        output_format        = AppGlobal.gui.ddw_format.get_text( )

        try:

            msg     = f"Begin select using file: {input_file_name} for format {output_format}"
            AppGlobal.gui_write_start( msg )
#            msg     = ( "\n"*2 + f"========= bcb_select_using_file(  ) =================" )
#            AppGlobal.print_debug( msg )
#            msg     = f"bcb_select_using_file args {db_file_name} {input_file_name} {output_format} db >>{db_file_name}<<"
#            AppGlobal.print_debug( msg )

            table_info    = db_objects.TableInfo( )
            table_info.build_select_info_from_file( db_file_name, input_file_name, print_out=False )
            table_info.build_select_sql_from_info(   )

            AppGlobal.print_debug( table_info )

            select_writer, fileout_name    = file_writers.make_file_writer( table_info, output_format, )

            table_access      = db_objects.TableAccess( db_file_name, table_info )

            table_access.run_info_sql_with_writer( select_writer )

            self.change_table_name( table_info.table_name )

            if  output_format == "py_log":
                self.os_open_text_file( self.parameters.pylogging_fn   )
                self.change_last_output_file_name( self.parameters.pylogging_fn )
            elif  output_format == "html":
                webbrowser.open( fileout_name, new=0, autoraise=True )
                self.change_last_output_file_name( fileout_name )
            else:
                self.os_open_text_file( fileout_name  )   # or log
                self.change_last_output_file_name( fileout_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Select complete"
        AppGlobal.gui_write_finish( msg )
    # ----------------------------------
    def bcb_select_all(self ):  # place holder for function not yet determined
        """

        """
        print( "bcb_select_all"  )

        db_file_name         = AppGlobal.gui.get_db_file_name()
        output_format        = AppGlobal.gui.ddw_format.get_text( )
        table_name           = AppGlobal.table_name    # is valid or get from control
        table_name           = AppGlobal.gui.ddw_tables.get_text()

        try:

            msg     = f"Begin select all: for table {table_name} and format {output_format}"
            AppGlobal.gui_write_start( msg )
            table_info    = db_objects.TableInfo( )

            table_info.build_select_info_all( db_file_name, table_name, print_out=False )
            table_info.build_select_sql_from_info(   )
            #table_info.build_select_info_from_file( db_file_name, input_file_name, print_out=False )

#            build_select_sql_from_file( db_file_name, input_file_name, print_out=False )  # needed for file writer

            AppGlobal.print_debug( table_info )

            select_writer, fileout_name    = file_writers.make_file_writer( table_info, output_format, )

            table_access      = db_objects.TableAccess( db_file_name, table_info )
            table_access.run_info_sql_with_writer( select_writer )

            self.change_table_name( table_info.table_name )

            self.open_output_file( fileout_name, output_format, set_last = True )
            # was code below

#            if  output_format == "py_log":
#                self.os_open_text_file( self.parameters.pylogging_fn   )
#                self.change_last_output_file_name( self.parameters.pylogging_fn )
#            elif  output_format == "html":
#                webbrowser.open( fileout_name, new=0, autoraise=True )
#                self.change_last_output_file_name( fileout_name )
#            else:
#                self.os_open_text_file( fileout_name  )   # or log
#                self.change_last_output_file_name( fileout_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Select complete"
        AppGlobal.gui_write_finish( msg )

    # ----------------------------------
    def bcb_insert_file( self ):  # place holder for function not yet determined

        text_file_name     = AppGlobal.filein_name
        db_file_name       = AppGlobal.gui.get_db_file_name()
        #fileout_name       = self.add_data_path( self.parameters.fileout_name )
        msg     = f"Insert from file {text_file_name}"
        AppGlobal.gui_write_start( msg )
        try:
            file_reader        = file_readers.FileReader( text_file_name, db_file_name,     )
            file_reader.insert_from_file(  )
            self.change_table_name( file_reader.table_info.table_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug(       exception.msg )
            AppGlobal.gui_write_error(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn )

        msg     = "Insert from file complete"
        AppGlobal.gui_write_finish( msg )

    # ----------------------------------
    def bcb_make_table_form(self ):
        """
        button call back, see name
        """
        try:
            db_file_name      = AppGlobal.gui.get_db_file_name()
            table_name        = AppGlobal.table_name
            msg  = f"Make Table Form ( data input form ) for {table_name}"
            AppGlobal.gui_write_start( msg )
            output_file_name  = add_data_path( f"input_form_for_{table_name}.txt" )
            #self.test_make_table_form( db_file_name,  table_name, output_file_name )

            table_access  = db_objects.TableAccess( db_file_name,  None )
            table_access.init_table_info_from_db( table_name )
            table_access.make_table_form( output_file_name )
            self.change_last_output_file_name( output_file_name )
            self.os_open_text_file( output_file_name  )

        except db_objects.DBOjectException as exception:
            AppGlobal.print_debug( exception.msg )
            self.gui_write_error(  exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg  = "Make Table Form done"
        AppGlobal.gui_write_finish( msg )

    #-----------------------------------
    def bcb_make_select_form (self ):
        """
        button call back, see name
        """
        table_name       = AppGlobal.table_name
        db_file_name     = AppGlobal.gui.get_db_file_name()

        msg  = f"Make Select Form for table {table_name}"
        AppGlobal.gui_write_start( msg )

        output_file_name = f"{table_name}_select_form.txt"
        output_file_name = add_data_path( output_file_name )
        try:
            table_info       = db_objects.TableInfo( )
            table_info.build_from_db_file( table_name, db_file_name, print_out=False )

            file_writer     = file_writers.SelectFormWriter( output_file_name, table_info )
            file_writer.write_file()
            self.change_last_output_file_name( output_file_name )
            self.os_open_text_file( output_file_name   )

        except db_objects.DBOjectException as exception:
            AppGlobal.print_debug( exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg  = "Done Make Select Form"
        AppGlobal.gui_write_finish( msg )


    # ----------------------------------
    def bcb_insert_file( self ):  # place holder for function not yet determined

        text_file_name     = AppGlobal.filein_name
        db_file_name       = AppGlobal.gui.get_db_file_name()
        #fileout_name       = add_data_path( self.parameters.fileout_name )
        msg     = f"Insert from file {text_file_name}"
        AppGlobal.gui_write_start( msg )
        try:
            file_reader        = file_readers.FileReader( text_file_name, db_file_name,     )
            file_reader.insert_from_file(  )
            self.change_table_name( file_reader.table_info.table_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug(       exception.msg )
            AppGlobal.gui_write_error(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn )
            msg     = "Exception for Insert from file complete"
            AppGlobal.gui_write_finish( msg )
        msg     = "Insert from file complete"
        AppGlobal.gui_write_finish( msg )


    # ----------------------------------
    def bcb_delete_all(self ):
        """
        calls functions that need to be written
        note early return
        """
        #print( "bcb_delete_all"  )

        db_file_name         = AppGlobal.gui.get_db_file_name()
        table_name           = AppGlobal.gui.ddw_tables.get_text()

        continue_flag  = messagebox.askokcancel("Delete All", f"Are you sure you want to delete all from {table_name}?")
        print(f"{continue_flag}")
        if not continue_flag:
            return

        try:
            msg     = f"Begin delete all: for table {table_name}"  # add confirm box
            AppGlobal.gui_write_start( msg )

            table_info    = db_objects.TableInfo( )

            table_info.build_select_info_all( db_file_name, table_name, print_out=False )
            table_info.build_delete_sql_from_info(   )
            AppGlobal.print_debug( table_info )

            table_access      = db_objects.TableAccess( db_file_name, table_info )
            table_access.run_info_sql_delete(  )

            self.change_table_name( table_info.table_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Delete All complete"
        AppGlobal.gui_write_finish( msg )

    # ----------------------------------
    def bcb_delete_all_old(self ):  # place holder for function not yet determined
        pass
        print( "bcb_delete_select" )

        db_file_name         = AppGlobal.gui.get_db_file_name()
        input_file_name      = AppGlobal.select_file_name

        try:

            msg     = f"Begin delete using file: {input_file_name}"
            AppGlobal.gui_write_start( msg )

            table_info    = db_objects.TableInfo( )
            table_info.build_select_info_from_file( db_file_name, input_file_name, print_out=False )
            table_info.build_select_sql_from_info(   )


            #table_info.build_select_sql_from_file( db_file_name, input_file_name, print_out=False )  # needed for file writer
            table_info.build_delete_sql_from_file( db_file_name, input_file_name )
            AppGlobal.print_debug( table_info )

            table_access      = db_objects.TableAccess( db_file_name, table_info )
            table_access.run_info_sql(  )

            self.change_table_name( table_info.table_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Delete complete"
        AppGlobal.gui_write_finish( msg )

    # ----------------------------------
    def bcb_delete_select(self ):  # place holder for function not yet determined
        """
        what it says
        delete selected records
        """
        #print( "bcb_delete_select" )

        db_file_name         = AppGlobal.gui.get_db_file_name()
        input_file_name      = AppGlobal.select_file_name

        continue_flag        = messagebox.askokcancel("Delete All", f"Are you sure you want to delete \nselected records from {db_file_name}?")
        #print(f"{continue_flag}")
        if not continue_flag:
            return

        try:
            msg     = f"Begin delete using file: {input_file_name}"
            AppGlobal.gui_write_start( msg )

            table_info    = db_objects.TableInfo( )
            table_info.build_select_info_from_file( db_file_name, input_file_name, print_out=False )
            table_info.build_delete_sql_from_info()
            AppGlobal.print_debug( table_info )

            table_access   = db_objects.TableAccess( db_file_name, table_info )
            table_access.run_info_sql_delete()

            self.change_table_name( table_info.table_name )

        except db_objects.DBOjectException as exception:
            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Delete complete"
        AppGlobal.gui_write_finish( msg )

    # ----------------------------------
    # --------- end bcb  -------------------------
    # ----------------------------------
    def tbd_bcb_wf_2( self ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_wf_2" + " check_file" )

        self.try_check_file(  AppGlobal.filein_name,
                              AppGlobal.gui.get_db_file_name(), )

    #--------------------------------
    def try_check_file( self, file_name_to_check, db_file_name ):
        """
        checks against the data_info, table must have been defined first
        uses
        """
        msg    = f"========= try_check_file( file_name = {file_name_to_check} ) ================="
        print(  "\n"*2 + msg )
        AppGlobal.write_gui( msg  )

        file_reader     = file_readers.FileReader( file_name_to_check, db_file_name,   )      #FileReader
        #file_reader.data_object.set_data_info( AppGlobal.data_info )

        errors   =  file_reader.check_file( 10 )   # 10 is max errors
        msg      = f"check insert file, errors = {errors}"
        AppGlobal.write_gui( msg  )

    # ----------------------------------
    def tbd_bcb_wf_4(self ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_wf_4" + "select" )
#        self.setup_table_info()
#        AppGlobal.print_info()
        self.try_select( add_data_path( self.parameters.db_file_name ),
                                  self.parameters.default_table_name,
                                  self.add_data_path( self.parameters.fileout_name  )
                                  )

    #--------------------------------
    def try_select( self,  db_file_name, table_name, fileout_name ):
        """
        select controlled by various items coded here, not by a file
        db and table must be defined first
        SelectLogWriter
        """
        print( "\n"*2 + f"========= try_select_all_rows( {db_file_name} {table_name} {fileout_name} ) =================")
        #AppGlobal.print_info()

        select_writer     = file_writers.SelectLogWriter(    None,         None )
        select_writer     = file_writers.SelectExportWriter( fileout_name, None )

        table_access      = db_objects.TableAccess( db_file_name,  None )
        table_access.init_table_info_from_db( table_name )

#        -----0 with select all
#        table_access.select_all_cols( sql_where, values_where,  order_by = "", select_writer = select_writer )

       #----- select all a different way with order by
        sql_where        = ""
        values_where     = tuple( [] )
        #        ORDER BY column1, column2, ... ASC|DESC;    , "ORDER BY bot_name ASC",  ....more...
        order_by         = "ORDER BY bot_name ASC"
        order_by         = ""
        table_access.select_all_cols( sql_where, values_where,  order_by = order_by, select_writer = select_writer )


        self.os_open_text_file( fileout_name  )   # or log

    # ----------------------------------
    def tbd_bcb_wf_6(self ):
        """
        select controlled by a file
        """
        pass
        print( "tbd_bcb_wf_6 select controlled by a file" )

    # ----------------------------------
    def tbd_bcb_wf_7(self ):
        """
        edit part a
        """
        msg     = "tbd_bcb_wf_7 edit part a"
        print( msg )
        AppGlobal.write_gui( msg  )
        self.test_edit_one_record_a( add_data_path( self.parameters.db_file_name ),
                        "mush", "Cantharellus lateritius",
                        add_data_path( self.parameters.edit_file_name ) )

    # ----------------------------------
    def test_edit_one_record_a( self, db_file_name, table_name, record_key, edit_file_name ):
        """

        """
        msg   = "\n"*2 +f"========= test_edit_one_record_a( {db_file_name}, {table_name}, {record_key} )  "
        print( msg )
        AppGlobal.write_gui( msg  )

        print( f"  ==========  try_edit_one_record_b( {edit_file_name},   ) =================")

        table_access  = db_objects.TableAccess( db_file_name,  None )
        table_access.init_table_info_from_db( table_name )

        table_access.edit_one_record_1( record_key, edit_file_name )

    # ----------------------------------
    def tbd_bcb_wf_8(self ):
        """
        edit part a
        """
        msg  = "tbd_bcb_wf_8 edit part b"
        print( msg )
        AppGlobal.write_gui( msg  )
        self.test_edit_one_record_b(  )

    # ----------------------------------
    def test_edit_one_record_b( self,  ):
        """
        ?? args are probably ng -- no ok now
        """
        msg     = "\n"*2 + f"========= edit_one_record_b( )  "
        print( msg )
        AppGlobal.write_gui( msg  )
        #print( f"          try_edit_one_record_1( {edit_file_name},                  ) =================")
        table_access  =  AppGlobal.table_access_for_edit
        table_access.edit_one_record_2(  )

    # ----------------------------------
    def for_bcb_wf_8( self,  ):
        """
        ?? args are probably ng -- no ok now
        """
        msg     = "\n"*2 + f"========= edit_one_record_b( )  "
        AppGlobal.print_debug( msg )
        AppGlobal.write_gui( msg  )

    # ---------------------- temp buttons
    # ----------------------------------
    def tbd_bcb_1(self ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_1" )
        AppGlobal.write_gui( "tbd_bcb_1" )

    # ----------------------------------
    def tbd_bcb_2( self, ):  # place holder for function not yet determined
        """
        """
        msg    = "tbd_bcb_2"
        AppGlobal.print_debug( msg )

#        self.test_new_select(  AppGlobal.gui.get_db_file_name(),
#                               AppGlobal.select_file_name,
#                             )

     # ----------------------------------
    def test_new_select( self, db_file_name, input_file_name ):
        """
        """
        msg     = f"args {db_file_name} {input_file_name}"
        AppGlobal.print_debug( msg )
        table_info    = db_objects.TableInfo( )
        # ------- choose one ---------------------------------
#        table_info.build_select_info_from_file( db_file_name, input_file_name, print_out=False )
        table_info.build_select_sql_from_file(  db_file_name, input_file_name, print_out=False )

        print( table_info )

    # ----------------------------------
    def tbd_bcb_3(self ):  # place holder for function not yet determined
        pass
#        print( "tbd_bcb_3" )
#        self.try_get_file_info( self.add_data_path( "mushroom_1.txt" ),  )
        self.build_from_input_file( add_data_path( self.parameters.filein_name  ),   )

    #--------------------------------
    def build_from_input_file( self, input_file_name,   ):
        """
        prob not needed this makes the sql from an input file but does not apply it
        """
        try:
            table_info  = db_objects.TableInfo( )
            table_info.build_from_input_file( input_file_name  )
            print( table_info )
            # next is additional test
            print( table_info.to_sql() )
            self.os_open_text_file( self.parameters.pylogging_fn   )
        except db_objects.DBOjectException as exception:
            print( exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

    # ----------------------------------
    def tbd_bcb_4a(self ):  # place holder for function not yet determined
        msg     = "tbd_bcb_4a"
        print( msg )
        AppGlobal.write_gui( msg  )
        self.try_edit_one_record_1(  add_data_path( self.parameters.db_file_name ),
                        "mush", "Cantharellus lateritius",
                        add_data_path( self.parameters.edit_file_name ) )

    # ----------------------------------
    def try_edit_one_record_1( self, db_file_name, table_name, record_key, edit_file_name ):
        """

        """
        msg   = "\n"*2 +f"========= try_edit_one_record_1( {db_file_name}, {table_name}, {record_key} )  "
        print( msg )
        AppGlobal.write_gui( msg  )
        print( f"          try_edit_one_record_1( {edit_file_name},                  ) =================")

        table_access  = db_objects.TableAccess( db_file_name,  None )
        table_access.init_table_info_from_db( table_name )   # convienienbce

         # SORT OF USED TO WORK
#        table_access  = db_objects.TableAccess( db_file_name,  None )
#        table_access.init_table_info_from_db( table_name )
#        #table_access.table_to_table_info( table_name )
        table_access.edit_one_record_1( record_key, edit_file_name )

        #try_edit_one_record_2( table_access  )

#try_edit_one_record_1(  add_data_path( parameters.db_file_name ),
#                        "mush", "amanita_ocreata", add_data_path( parameters.edit_file_name ) )
    # ----------------------------------
    def tbd_bcb_4b(self ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_4b" )

        self.try_edit_one_record_2(  )

    # ----------------------------------
    def try_edit_one_record_2( self,  ):
        """
        ?? args are probably ng -- no ok now
        """
        print("\n"*2)
        print( f"========= edit_one_record_2( )  ")
        #print( f"          try_edit_one_record_1( {edit_file_name},                  ) =================")
        table_access  =  AppGlobal.table_access_for_edit
        table_access.edit_one_record_2(  )

    # ----------------------------------
    def tbd_bcb_5(self ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_5" )

    # ----------------------------------
    def tbd_bcb_6(self ):  # place holder for function not yet determined
        print( "tbd_bcb_6" )

    # ----------------------------------
    def tbd_bcb_7( self, ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_7" )

    # ----------------------------------
    def tbd_bcb_8( self, ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_8" )

    # ----------------------------------
    def tbd_bcb_9( self, ):  # place holder for function not yet determined
        print( "tbd_bcb_9" )


    # ----------------------------------
    def tbd_bcb_10( self, ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_10" )

    # ----------------------------------
    def tbd_bcb_11( self, ):  # place holder for function not yet determined
        pass
        print( "tbd_bcb_11" )


 #--------------------------------
if __name__ == "__main__":

    app = App( )

# ======================= eof =======================






