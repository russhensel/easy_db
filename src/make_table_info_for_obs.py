# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 20:19:45 2018

@author: Russ
"""

import all_so_far
from   app_global import AppGlobal

AppGlobal.print_info()

a_table_info                  = all_so_far.TableInfo()

a_table_info.table_name        = "room"
a_table_info.table_key         = "key"
a_table_info.table_data_dict   =  {'key': 'TEXT', 'bot_name': 'TEXT', 'common_name_1': 'TEXT', 'common_name_2': 'TEXT',
                                   'cap_color': 'TEXT', 'cap_dia_min': 'TEXT', 'cap_dia_max': 'TEXT', 'cap_dia': 'TEXT',
                                'cap_shape': 'TEXT', 'veil': 'TEXT', 'flesh_color': 'TEXT', 'foot_color': 'TEXT',
                                'gill_color': 'TEXT', 'ring_color': 'TEXT', 'volva': 'TEXT', 'odour': 'TEXT', 'eat': 'TEXT',
                                'found_where': 'TEXT', 'seasons': 'TEXT', 'web_link_1': 'TEXT', 'looks_like_1': 'TEXT',
                                'looks_like_2': 'TEXT', 'stain': 'TEXT', 'cap_edge': 'TEXT', 'cap_surface': 'TEXT',
                                'gills': 'TEXT', 'stalk': 'TEXT', 'cap': 'TEXT', 'cap_bruse': 'TEXT', 'geo': 'TEXT',
                                'flesh': 'TEXT', 'spore_print': 'TEXT', 'aub_page': 'TEXT', 'spore': 'TEXT', 'stem': 'TEXT',
                                'spore_color': 'TEXT', 'note': 'TEXT', 'description': 'TEXT', 'bruse': 'TEXT'}


AppGlobal.all_table_infos[ a_table_info.table_name ] = a_table_info
AppGlobal.current_table   =a_table_info.table_name

AppGlobal.print_info()