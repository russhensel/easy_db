
D:\Russ\0000\python00\python3\_projects\...

pip install pypubsub




play around with sql lite reading from text files
for mushrooms observations people stoves

=================== history ==============================

    ** Ver2  oct 3   -- move all code but global to all_so_far.py -- was working but broke it
    ** Ver3  re factor add parameters, slim down app global
    ** Ver3a for trip
    ** Ver5  back from trip
    ** Ver5a  back from trip - do some deletes
    ** Ver6  back from trip -- lots more working lets delete again, edit half way plus some there
    ** Ver7  gui almost usable
    ** add a data dir to move action around and make easy to backup
    ** work on minimum gui -- use text editor as forms....
    ** add table name to top of data file
    ** py logging works, implement and then delete many of the prints -- also use print_debug( msg_debug )
    ** move all trys to new module
    ** all output should probably go thru select_where which should have a sort added
    ** delete_all - delete_where both seem to work now
    ** db_objects at 1884 lines, should we re factor  got rid of 100 so far
    ** what if multiple : in an input line -- bad thing happen - may be fixed with split( ":", 1)
    ** edit file on input panel
    ** get rid of column TEXT
    ** field defines .... trim ends, change multiple spaces to single space and space to _ ( bug ng now ) lower case it

    *! update_with_row_object -- sort of where we left off
    *! select from example works, need to use values?? and order by and choose columns
    *! more export formats, and switch between them   csv, sql format
        ** export via csv ( make only tab separated ) one of the outputs
        !! make an sql syntax export
    *! review !! and ?? in code

    *! work on help for first use with example file
    *! need to add second thread, or yield to gui. right now yield works
    *! load file names from parameters
    *! start printing results to text area
    *! need blank form  and example blank form for define activities  -- still needs error catch and messages
        make forms with buttons from dbname and table name
    !! drop table button
    !! fix html output
    !! work on edit form new data old data,......
    !! simple search edit one field   -- use Like Starting with Ending with Contains in simple and in form based
    !! work on key field consider the one in sql lite, making an auto one, and having user define one ?? how
    !! allow search and update or at least update with multiple records, may have to use 2 row objects per update
        and match them up
    !! select_from_dict -- finish if we have a purpose ??
    !! make run sql -- do some valid checks
    !! make a delete based on form for select -- change select to delete in the form -- show the select and ask ??
    !! easy delete of one record -- perhaps during an edit change purpose from edit to delete ??
    !! make a cut and paste from any select work as an edit -- still 2 steps ??  Or just find record and update.
    !! add processing comment to bottom of files -- move files
    !! bug -- blank record ( nothing between record breaks ) produces an error with poor error message
    !! easy order by ... and on the select all
    !! multiple sort by order in the select file - some may be there - test reording
    !! feedback on what is up
        *! comment on each record added -- now works, but needs improvement, record number perhaps
        !! all the others
        *! checked and insert, think checked not right yer
    !! exact select
    !! quick select
    !! sqlite internal key column   ROWID  include in selects delete from insert  -- include in updates use just for where
    !! add type hints on column definition rather than making all TEXT   cost_of_food( number )
    !! print as columns perhaps html
    *! more meta from db in file info and some conversion and formatting stuff
    !! display area not scrolling
    !! add last output file field and an edit button for it
    !! after define table, update the table list
    !! add form modify column
    !! add form sql select
    !! add form sql ( no select )

    *! create new db
    !! use sql alchemy



========================== new gui elements =======================

areas??

General   data dir and database file     --- data_dir will be in same directory as the db file or from input file   buttons: nopne
Input     file name and action buttons
Select/Delete  file_name or text for simple and action buttons -- delete is a 2 step like edit
Edit

field for last table, field for last output file

===========================================================

do we need a publish subscribe ....

values in the gui area

do we have a value changed event - - assume yes   -- or trigger on a get event

beware of circular calls -- may work best with classess who can set an ignore event
set_ xxxx( value or none )
    set_val
    if value none then
         set_val = get from gui
         else
         set into gui set_val
    next
        call all who care, not me ... not one which will become circular


assume call from class

    call set_value ....
        ignore subscribe topic
        set_val( set_val )



      callback on topic
          if ignore_setval_xxxxx
              return
       else
          do whatever













========================= snips =============================

     #  AppGlobal.logger.log( logging.DEBUG, msg )   AppGlobal.logger.log( AppGlobal.force_log_level, msg )

db_objects>error msg
             msg     =  f"file purpose should be 'insert' but is {file_purpose}"
             print_debug( debug_msg )
             print( msg )
             AppGlobal.logger.debug( msg )
             raise db_objects.DBOjectException( msg )

 db_objects>error exception
        try:
            table_info   = db_objects.TableInfo( )


        except db_objects.DBOjectException as exception:
            print( exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )
        print( "alldone" )


db_objects>method
   #--------------------------------
    def select_all_cols( self, sql_where, values_where, order_by, select_writer ):
        """
        Purpose:    see name
                    sql_where  string like  "WHERE symbol=?"  ...... ? is to subtistute value

        Args:       see names sql_where, values_where, order_by, select_writer = a file_writer to write the output
        Returns:    zip changes internal state
        Raises:     db_objects.DBOjectException  // none planned but could have bad sql....
        """

db_objects>function

    # -----------------------------------
    def foo( self, ):
        """
        Purpose:
            see name
        Args:
            n: the number to get the square root of.
        Returns:
            the square root of n.
        Raises:
            TypeError: if n is not a number.

        """
        return ret_val

    #----------- debug -----------
    #----------- main functions -----------







================= details ============================

issue: what object depends on what, in what order do we boot them up?

        current thinking  not quiet up to date
                Obj Type     instance stuff
                AppGlobal    sets the context, may have old TableInfo items
                TableInfo    Holds info on a table, needs to have:


                bring up first
                FileReaderToDataDict > FileReaderToTableInfo     creates a table info from example.... stores in AppGlobal
                                       seems to need a table access ... why
                TableAccess  args:  TableInfo and db_file_name later perhaps  select a place to optput - a file out and a where file in
                FileReader   needs a Table info file to RowObject can create its own from a TableAccess( which has a TableInfo )
                FileLineReader
                RowObject    needs a Table info perhaps



issue: how do we edit:
       lets do this:  kick out file
       have an update much like the import

            grab the key, get data into a dict -- if no data then we are in add mode
            read the file get data into another dict.
            compare the two dicts and apply the update

    or a crude way is to delete the data then insert -- might start with it, could even change the key of old
    then insert the new???? -- already working on the selective update

    need more meta info in AppGlobal,
    appGlobal: use as a prototype for GUI add GUI later -- gui now added
    call of subroutine = button push

    how do we stop part way thru ... throw my own except and catch it at the top -- it needs a message and
    perhaps a full stack trace, although we need to think about other exceptions

    make a connection object and keep it connected??
    make a refine database form to change types

    how to deal with keys columns ??

    for two step have button label change    step 1 step 2 and cancel

================= gui ====================

directory                   selectable
database file name          selectable, type-able
table name                  selectable, type-able
input file name             selectable
select criteria file name   selectable
output file name            selectable, type-able
output file format          selectable

make a control for above

buttons: consider put data on buttons ... big button but so
        define_table
        check input file
        insert file
        select all
        delete all
        select using select file


        select using sql   use select file but pluck out sql no chooing of columns yet

export table definition
export in sql format






============================================ scratch -- clean me up ===========================
self.change_table_name( table_info.table_name )
self.change_last_output_file_name( output_file_name )
            self.os_open_text_file( output_file_name   )

    # ----- define table  ready for rename !!
    def tbd_bcb_wf_1(self ):  # place holder for function not yet determined

#        msg   = "\n"*2 +"tbd_bcb_wf_1 define_table"
#        print( msg )
#        self.write_gui( msg  )
#        self.try_make_and_run_table_def_sql( self.add_data_path( self.parameters.db_file_name ),
#                                             self.add_data_path( self.parameters.filein_name ), )

        db_file_name  = AppGlobal.db_file_name
        in_file_name  = AppGlobal.filein_name

        msg  = "Define Table"
        _gui_start( msg )
        try:
            table_info   = db_objects.TableInfo( )
            table_info.build_from_input_file( in_file_name,  print_out=True )
            #print( table_info.to_sql() )
            table_access  = db_objects.TableAccess( db_file_name, table_info  )
            table_access.define_table_from_table_info()
            # success  # refresh db file
            temp        = AppGlobal.db_file_name
            AppGlobal  = None
            self.change_db_file_name( temp )

            msg    = f"... new table defined: {table_info.table_name}"
            self.write_gui( msg )
        except db_objects.DBOjectException as exception:
            print( exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg  = "Define Table"
        _gui_finish( msg )


                try:

            msg     = f"Begin select using file: {input_file_name} for format {output_format}"
            AppGlobal.gui_write_start( msg )
#            msg     = ( "\n"*2 + f"========= bcb_select_using_file(  ) =================" )
#            AppGlobal.print_debug( msg )
#            msg     = f"bcb_select_using_file args {db_file_name} {input_file_name} {output_format} db >>{db_file_name}<<"
#            AppGlobal.print_debug( msg )

            table_info    = db_objects.TableInfo( )

            table_info.build_select_sql_from_file( db_file_name, input_file_name, print_out=False )  # needed for file writer

            AppGlobal.print_debug( table_info )

            output_format   = AppGlobal.gui.ddw_format.get_text()
            if  output_format == "py_log":
                fileout_name      = self.add_data_path( "select_output.txt"  )
                select_writer     = file_writers.SelectLogWriter(    None,         table_info )

            elif output_format == "input":
                fileout_name      = self.add_data_path( "select_output.txt"  )
                select_writer     = file_writers.SelectExportWriter( fileout_name, table_info )

            elif output_format == "csv":
                fileout_name      = self.add_data_path( "select_output.csv"  )
                select_writer     = file_writers.SelectCSVWriter( fileout_name, table_info )

            elif output_format == "table":
                fileout_name      = self.add_data_path( "select_output_table.txt"  )
                select_writer     = file_writers.SelectTableWriter( fileout_name, table_info )

            else:
                msg   =  f"invalid output_format = {output_format}"
                AppGlobal.write_gui( msg )
                AppGlobal.print_debug( msg )
                raise db_objects.DBOjectException( msg )

            table_access      = db_objects.TableAccess( db_file_name, table_info )

            table_access.run_info_sql_with_writer( select_writer )

            if  output_format == "py_log":
                self.os_open_text_file( self.parameters.pylogging_fn   )
                self.change_last_output_file_name( self.parameters.pylogging_fn )
            else:
                self.os_open_text_file( fileout_name  )   # or log
                self.change_last_output_file_name( fileout_name )

        except db_objects.DBOjectException as exception:

            AppGlobal.print_debug( exception.msg )
            AppGlobal.write_gui(   exception.msg )
            self.os_open_text_file( self.parameters.pylogging_fn   )

        msg     = "Select complete"
        AppGlobal.gui_write_finish( msg )
#        AppGlobal.print_debug( "alldone test_select_using_file" )






