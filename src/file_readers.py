# -*- coding: utf-8 -*-

# ------------ local
from   app_global import AppGlobal
import db_objects



#-----------------------------------------
def is_ignored_line( a_line ):
    """
    if true then ignore the line in the file -- for column values
    assumes white space spaces already have been removed
    """
    if a_line == "":
        return True

    ignore_list  = [ "#",   ]  # "use_table:"
    for i_ignore in ignore_list:
        if a_line.startswith( i_ignore ):
            return True
    return False

#-----------------------------------------
def make_name_ok( name ):
    """
    normalizes and validates
    return ( ok_flag, new_name )
    is_ok, new_name  = make_name_ok( new_name )
    may be wrong time to check, this whole area looks like it was in the middle of a refactor not finished for now skip most stuff
    """
    new_name         = normalize_name( name )
#    is_ok            = db_objects.begin_with_alpha( new_name )
#    is_ok, new_name  = db_objects.is_reserved_word( new_name )
#    return ( is_ok, new_name )
    return ( True, new_name )

#-----------------------------------------
def normalize_name( name ):
    """
    fixes cap and white space......, is there more
    perhaps return a flag if invalid !!
    """
    name        = name.lower()              # names must be lower case
    name        = name.replace( "-", " " )  # treat as space eliminate later
    name        = " ".join( name.split() )  # white space to one
    name        = name.replace( " ", "_" )

    return name

#====================================
class FileReader( object ):
    """
    this version from  file_to_db

    -- init with a file_name, or a table name but None means get from file ??

    Arg:    file_name_to_read,    might use as argument to ones of the functions
            db_file_name,         not sure it needs this may get from file
                                  may be used to build a file_info as in check_file
            purpose               why we are reading the file !! some calls may need update
                                  edit_data
                                  input_output

    """
    #  ----------- init -----------
    def __init__( self,  file_name_to_read, db_file_name, ):       #FileReader

        self.file_name_to_read       = file_name_to_read

        self.db_file_name            = db_file_name
        AppGlobal.print_debug( f"FileReader for {file_name_to_read} for database {db_file_name}",  )

    #-----------------------------------------
    def insert_from_file( self,  ):
        """

        Return: boolean


        """
        inserts    = 0
        msg   = "insert_from_file"
        AppGlobal.print_debug( msg )
        #AppGlobal.write_gui( msg )

        # can some be factored out ??

        file_line_reader     = FileLineReader( self.file_name_to_read )
        file_line_reader.get_file_info( )  # !! look at return reason
        is_ok = file_line_reader.check_for_required_file_info( [ "purpose", "use_table"] )
        if not is_ok:
            msg = f"insert_from_file  {self.file_name_to_read} missing info about purpose or table  "
            #AppGlobal.print_debug( msg, )
            AppGlobal.write_gui_error( msg )
            raise db_objects.DBOjectException( msg )

        self.dict_file_info           = file_line_reader.file_info_dict
        file_purpose                  = self.dict_file_info[ "purpose" ]
        if file_purpose != "insert":
            msg     =  f"file purpose should be 'insert' but is {file_purpose}"
            AppGlobal.write_gui_error( msg )
            #AppGlobal.print_debug( msg )
            raise db_objects.DBOjectException( msg )

        table_name                  = self.dict_file_info[ "use_table" ]

        self.table_info      = db_objects.TableInfo( )
        self.table_info.build_from_db_file( table_name, self.db_file_name,  print_out=False  )

        row_object           = db_objects.RowObject( self.table_info )

        self.table_access    = db_objects.TableAccess(  self.db_file_name, self.table_info )
        values_added         = False
        while True:     # will the line have a newline... on it yes need to strip off end
            what, column, data = file_line_reader.get_next_line()
            #print( what, column, data )
            if what == "eof":
                break

            if what == "break" and values_added:
                msg       = f"insert record: num. inserted = {inserts}"
                msg_d     = msg + str( row_object )
                AppGlobal.print_debug( msg_d )
                AppGlobal.gui_write_progress( msg )

                inserts   += 1
                #self.table_access.update_with_data_object( self.data_object )
                self.table_access.insert_with_row_object( row_object )
                values_added  = False
                row_object.clear_edit_dict()
            else:
                values_added   = True
                is_reserved, the_word = db_objects.is_reserved_word( column )
                if is_reserved:
                    AppGlobal.print_debug( f"problem on line {self.file_line_reader.ix_line} = {self.file_line_reader.i_line} word is{the_word}" )
                    AppGlobal.write_gui_error( msg )
                    raise DBOjectException( msg )

#                msg_debug  =  f"at line {file_line_reader.ix_line} adding data*>{column}:{data}<*"
#                print( msg_debug )

                values_added   = True
                row_object.add_value( column, data, row_object.ix_new_value )

        msg       = f"===== insert record done: num. inserted = {inserts}"
        AppGlobal.print_debug( msg )
        # AppGlobal.write_gui( msg )
        # position file first in case not at end ??
        file_line_reader.add_footer( "#new footer material added by insert of records \n")

        file_line_reader.close_file()

 #-----------------------------------------
    def update_from_file( self, text_file_name, row_object ):
        """
        file should contain one record only -- check
        this is a new version using FileLineReader   test to see whre fails, compare to above
        Return:
            state of row_object which goes back to caller
        Raises:
            should probably !!
        """
        msg  = f"\n>>>>>>>update_from_file() {text_file_name}  {row_object} "
        AppGlobal.print_debug( msg )
        #AppGlobal.logger.debug( msg )

        # row_object     = RowObject( self.table_access   ) need the one from the export table access
        # edit_record_1
        #looks like incomplet, some objects to be updated.
        file_line_reader     = FileLineReader( text_file_name )
        file_line_reader.get_file_info( )   # !! look at return reason
        file_line_reader.check_for_required_file_info( [ "purpose", "use_table"] )

        #test insert first then add som missing code here.

        # finish header processing !!
        values_added  = False
        while True:
            what, column, data = file_line_reader.get_next_line()
            print( what, column, data )
            if what == "eof":
                break
            if what == "break" and values_added:
#                msg = "------ adding record: data -------\n" + str( row_object )
#                AppGlobal.print_debug( msg )
#                AppGlobal.logger.debug( msg )

                #self.table_access.update_with_data_object( self.data_object )
                #self.table_access.update_with_row_object( row_object )
                #values_added  = False
                break

            else:
                values_added   = True
                # assume column ?? or add error processing
#                msg   = f"update data:  {column}:{data}"
#                AppGlobal.print_debug( msg )

                row_object.add_value( column, data, row_object.ix_new_value )
#                if column bug== "bot_name":    # make key from bot_name value  !! needs better solution
#                    #key_value   = data_val.lower()
#                    data    = data.replace(" ", "_")
#                    row_object.add_value( "key", data, row_object.ix_new_value )

    #-----------------------------------------
    def check_file( self, max_errors ):
        """
        Purpose:    check file to see if consistent with a defined table in an existing database
                    !! check all end of check that might leave file open
        Purpose:    check the file against some TableInfo.  TableInfo may or may not be based
                    on reading the database ( or perhaps some other source )
        Args:       file_name_to_read, db_file_name, from init,
                    db table from file,
                    build my own table info from the db

                    need a file name and a table_info,

                    ** get and set table name from the file

        Return:     number of errors ??  may need tuple of something, ran to end, records checked record_errors
        Raises:     nothing in code
        """
        inserts   = 0
        msg       = f"check_file: {self.file_name_to_read} "
        AppGlobal.write_gui( msg )

        file_line_reader     = FileLineReader( self.file_name_to_read )
        file_line_reader.get_file_info( )   # !! look at return reason
        is_ok = file_line_reader.check_for_required_file_info( [ "purpose", "use_table"] )
        if not is_ok:
            msg = f"check_file: {self.file_name_to_read} "
            AppGlobal.print_debug( msg, )
            return -1

        self.dict_file_info           = file_line_reader.file_info_dict
        file_purpose                  = self.dict_file_info[ "purpose" ]
        if file_purpose != "insert":  # or edit??
            msg     =  f"file purpose should be 'insert' ?? but is {file_purpose}"
            AppGlobal.print_debug( msg )
            raise db_objects.DBOjectException( msg )

        # assume that table info is not populated build from the db

        self.table_name       = self.dict_file_info[ "use_table" ]
        a_table_info          = db_objects.TableInfo()
        is_ok                 = a_table_info.build_from_db_file( self.table_name, self.db_file_name, ) # print_out=True )
        # add error here !!
#        here is where I was last working
#        looks like row object has ref to tableaccess which I think it should not
#        also look at readme

        # now based on the db ( do we have one ) get a table info
        errors         = 0
        row_object     = db_objects.RowObject( a_table_info   )
        values_added   = False
        while True:     # will the line have a newline... on it yes need to strip off end
            what, column, data = file_line_reader.get_next_line()
            #print( what, column, data )
            if what == "eof":
                msg = f"------ eof: errors = {errors} at line {file_line_reader.ix_line}-------"
                AppGlobal.print_debug( msg )
                AppGlobal.write_gui( msg )
                file_line_reader.close_file()
                break

            if what == "break" and values_added:
                inserts   += 1
                msg       = f"===== checking file, records checked: = {inserts}, total errors = {errors}"
                msg_d     = msg + str( row_object )
                AppGlobal.print_debug( msg_d )
                AppGlobal.write_gui( msg )

                #self.table_access.update_with_data_object( self.data_object )
#                self.table_access.insert_with_row_object( row_object )
                values_added  = False
                row_object.clear_edit_dict()
            else:
                values_added   = True
                is_reserved, the_word = db_objects.is_reserved_word( column )
                if is_reserved:
                    msg = f"problem on line {self.file_line_reader.ix_line} = {file_line_reader.i_line} word is{the_word}"
                    AppGlobal.print_debug( msg )
                    errors    += 1
                    if errors > max_errors:
                        msg    = f"hit max errors {max_errors} at line {file_line_reader.ix_line}, I quit "
                        AppGlobal.print_debug( msg )
                        AppGlobal.write_gui( msg )
                        # raise db_objects.DBOjectException( msg )
                        return errors
                    break

                    return errors

#                msg  =  f"at line {file_line_reader.ix_line} checking data>>  {column}:{data}"  )

                values_added   = True
                is_ok = row_object.add_value( column, data, row_object.ix_new_value )
                if not is_ok:
                    msg = f"problem on line {file_line_reader.ix_line} = {file_line_reader.i_line} column name not in dict"
                    AppGlobal.print_debug( msg )
                    errors    += 1
                    if errors > max_errors:
                        file_line_reader.close_file()
                        msg    = f"hit max errors {max_errors} at line {file_line_reader.ix_line}, I quit "
                        AppGlobal.print_debug( msg )
                        #AppGlobal.logger.debug( msg )
                        #raise db_objects.DBOjectException( msg )
                        return errors

        msg       = f"===== check file done, records checked: = {inserts}, total errors = {errors}"
        AppGlobal.print_debug( msg )
        AppGlobal.write_gui( msg )
        file_line_reader.close_file()
        return errors

#====================================
class FileLineReader( object ):
    """
    normally this should be called to read any file containing:
    data definition
    data
    where specifications
    order by specifications

    Created by FileReader and FileReaderToDataDict

    -- init with a file_name,

    Arg:    file_name_to_read,


    """
    #  ----------- init -----------
    def __init__( self,  file_name_to_read, ):       #FileReader
        """
        Return": see open_file
        """
        self.file_name_to_read       = file_name_to_read
        msg = f"FileLineReader() for {file_name_to_read}"
        AppGlobal.print_debug( msg,   )

        self.file_info_dict      = None      # populated later or not??    infor in the file not necess in table
        self.filein              = None      # populated later by open_file
        self.header_read_status  = "0"       # 0   no attempt to read, see code for others
        self.record_break        = ":===="   # may be shorther than the one written for safety
        self.ix_line             = 0  # use for error report
        self.i_line              = None   # used for error report
        self.open_file()

    #  ----------- init -----------
    def open_file( self,     ):
        """
        open the file, but called from init, generally you should not need to ccall
        Return:  internal but you can check .filein ==  None for failure
        Raises:  DBOjectException( msg )

        """
        try:
            self.filein         = open( self.file_name_to_read, "r+" )    #"r"  )   "r+" read write open at beginning
        except:
            #filein.close() if error this is undefined

            msg = f"FileLineReader.open_file for {self.filein} failed, how about improve error handeling"
            AppGlobal.print_debug( msg,  )
            AppGlobal.gui.write_gui( msg )
            raise db_objects.DBOjectException( msg )

            self.filein         = None
        self.ix_line            = 0        # for error report and .......

    #  ----------- init -----------
    def close_file( self,     ):       #FileReader
        """
        """
        try:
            self.filein.close()
        except:
            #filein.close() if error this is undefined

            msg = f"FileLineReader.close_file for {file_name_to_read} failed, how about improve error handeling"
            AppGlobal.print_debug( msg,   )

            self.filein         = None

    #  ----------- init -----------
    def add_footer( self, a_string    ):       #FileReader
        """
        here the reader writes to upata the use of the "input file"
        """
        self.filein.seek( 0, 2 )   # position to end i hope
        self.filein.write( a_string )



    #--------------------------------
    def get_next_line( self, ):
        """
        perhaps get_next_column_line in case we want other kinds
        are we a generator or just a function called with end
        indicator?? for now just a function
        Return tuple   (   )
        what, column, data = file_line_reader.get_next_line()
        """
#        self.ix_line   += 1
        #read_next       = True
        while True:     # need this or break out with break, continue return
            self.ix_line   += 1
            self.i_line    = self.filein.readline( ) # save for error report
#            msg    = f"get_next_line reading line {self.ix_line} "
#            AppGlobal.print_debug( msg,   )

            lineinprocess  = self.i_line

            lineinprocess  = lineinprocess.strip( " " )        # does what if \n on end  actually bad code !! just check on ""
            if len( lineinprocess ) == 0:
                return ( "eof", "", "" )    # eof comes when read does not include \n and is just ""

            lineinprocess  = lineinprocess.rstrip('\n')
            if lineinprocess.startswith( self.record_break ):    # begin of nex record
                return ( "break", "", ""  )
                #return 1/0

            if is_ignored_line( lineinprocess ):
                continue
            # extract key part split on :
            parts       = lineinprocess.split( ":", 1 )  # 1 split 2 parts
            # check that at least splits in 2 ?? and not more, can split be limited
            if len( parts ) < 2:
                # !! is this right or add a null part 2
                continue

            name        = normalize_name( parts[0] )
            data_val    = parts[1]
            # !! check this is what we want:
            data_val    = data_val.strip( " " )
            if data_val == "":
               data_val = "null string"
#            msg    =  f"get_next_line +>{name}:{data_val}<+"
#            print( msg )

            return ( "column", name, data_val  )
        # end of while loop, we should have broken out in some way

    #--------------------------------
    def check_for_required_file_info( self, req_list,   ):
        """
        Arg:  req_list  itterable of strings
        Return: True if ok
        """
        is_ok = True
        for i_key in req_list:
            if i_key not in self.file_info_dict:
                is_ok = False
                msg = f"required info for file >>{i_key}<< is missing"
                AppGlobal.print_debug( msg )

        return is_ok

    #--------------------------------
    def get_file_info( self, ):
        """
        or section header
        call just after file is opened, or after approiate break it will read the header and
        set up for the first record
        get a dict of meta data from the first n lines
        of the file
        error checking comes later ??? true
        Old Return: the dict
        my return from middle !!
        Return:
                  state
                          info_dict
                          self.header_read_status

                  why return which is self.header_read_status
                                 0
                                 ...         in process
                                 ok      -- read it     till break could be null
                                 eof     -- hit end of file may or may not have a complete dict could be null
                                 break   -- either may be ok
                                 max     -- hit max lines

        # why not using get_next_line   -- looks like factored out but not implemented ??
        """
        max_line                  = self.ix_line  + 90
        self.file_info_dict       = {}
        self.file_info_dict["file_name"]  = self.file_name_to_read

        # self.ix_line              = 0    # use firstline = 1 set in get file_info
        self.header_read_status       = "..."
        # think if we enumerate the itterator is dead so we need to readline?
        #keep_reading              = True
        while True:

            if self.ix_line > max_line:
                msg_debug = f"FileLineReader hit max ix_line {self.ix_line} "
                AppGlobal.print_debug( msg_debug,   )
                AppGlobal.logger.debug( msg_debug )
                self.header_read_status  = "max"
                return self.header_read_status

#            msg    = f"get_file_info reading line {self.ix_line} "
#            AppGlobal.print_debug( msg,   )

            self.i_line     = self.filein.readline( )
            i_line          = self.i_line

            self.ix_line   += 1
#            print( i_line )
            if len( i_line ) == 0:
                self.header_read_status       = "eof"
                return self.header_read_status
            # perhaps should not clean yet ??
            i_line = i_line.rstrip('\n')
            i_line = i_line.strip( " " )
            if i_line.startswith( ":====" ):    # begin of record we are done with header
                self.header_read_status       = "break"
                return self.header_read_status

            if is_ignored_line( i_line ):
                continue

            parts       = i_line.split( ":" )
            # clean parts?  ?? no part 2 add a null
            meta_name   = parts[0]              # not db data in file but meta
            #!! think meta name needs to be checked for spaces perhaps uniuue ness ( or do we quitely overwrite ( yes )) reserved words, caps.....

            meta_val    = parts[1]
            # get rid of any comment in meta_val


            parts       = meta_val.split( "#" )
            meta_val    = parts[0]
            meta_val    = meta_val.strip( " " )

            is_ok, meta_name  = make_name_ok( meta_name )
            AppGlobal.print_debug( f".get_file_info meta_name >>{meta_name}<< meta_val >>{meta_val}<<")

            if not is_ok:
                "got insert which is purpose do we put in dict and check later ??"
                AppGlobal.print_debug( f"error in input file on line {self.ix_line}" )
                x = 1/0   # !!

            self.file_info_dict[ meta_name ]   = meta_val

        return "never get here unless break"

# =================================================

if __name__ == "__main__":
    #----- run the full app
    import  easy_db
    app   = easy_db.App(   )

# =================== eof ==============================












