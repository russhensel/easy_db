# -*- coding: utf-8 -*-


#import HTML
import html
import os
from   app_global import AppGlobal
import db_objects
"""
these objects write the results of a select in
various formats



"""

BREAK_LINE      =  ":===================="    # break in "input files"

# ----------------------------------------------
def add_data_path(  file_name ):
    """
    complete short path names by adding data_dir part  ... remove instance funct of same name
    """
    return os.path.join( AppGlobal.data_dir, file_name )


 # -------------------------------
def make_file_writer(  table_info, output_format,  ):
    """
    Purpose:
        see name -- make a file_writer of the correct format output_format
    Args:

    Returns:
        a fileWriter
    Raises:
        none local

    might want option for output file name, also return apprioiate editor ??

    """
    msg     = f"make_file_writer: for format {output_format}"
    AppGlobal.print_debug( msg )
    AppGlobal.gui_write_progress( msg )

    if  output_format == "py_log":
        fileout_name      = add_data_path( AppGlobal.parameters.pylogging_fn  )
        select_writer     = SelectLogWriter(    None,         table_info )

    elif output_format == "input":
        fileout_name      = add_data_path( "output_select.txt"  )
        select_writer     = SelectExportWriter( fileout_name, table_info )

    elif output_format == "csv":
        fileout_name      = add_data_path( "output_select.csv"  )
        select_writer     = SelectCSVWriter( fileout_name, table_info )

    elif output_format == "table":
        fileout_name      = add_data_path( "output_select_table.txt"  )
        select_writer     = SelectTableWriter( fileout_name, table_info )

    elif output_format == "html":
        fileout_name      = add_data_path( "output_select_table.html"  )
        select_writer     = SelectHTMLWriter( fileout_name, table_info )

    else:
        msg   =  f"invalid output_format = {output_format}"
        AppGlobal.write_gui( msg )
        AppGlobal.print_debug( msg )
        raise db_objects.DBOjectException( msg )

#    msg     = f"make_file_writer: return {( select_writer, fileout_name )}"
#    AppGlobal.print_debug( msg )

    return ( select_writer, fileout_name )

# --------------- Select Output Export Report -----------------------
class SelectExportWriter( object ):
    """
    Purpose: see title
    Args:

    for now make this writer out lines in order
    export is the same format as required for input with the pupose:export which needs
    to be changed if you want to input the file
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        see class doc
        """
        self.file_name       = file_name
        self.table_info      = table_info
        # more def in other functions

    #----------- init -----------
    def write_header(self, ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
#        msg   = "write header...."
#        AppGlobal.print_debug( msg )

        self.fileout                = open( self.file_name, "w" ) # error !!

        self.col_names              =  [ i_format[0] for i_format in self.table_info.format_list ]
        msg   = f"write_header()  self.col_names  {self.col_names}"
        AppGlobal.print_debug( msg )

        i_line    = f"#---------- file output from {AppGlobal.controller.app_name} {AppGlobal.controller.app_version}"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"# sql = {self.table_info.sql}"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"purpose:export"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"use_table: {self.table_info.table_name}"
        self.fileout.write( i_line   + "\n" )

    #----------------------
    def write_row(self, row_object ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
#        i_line    =  ":===================="
        self.fileout.write( BREAK_LINE  + "\n" )

        for ix_col, i_col in enumerate( self.col_names ):
            #i_data    = self.fix_null( row[ ix_col ] )

            i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            i_line    = f"{i_col}:{i_data}"
            self.fileout.write( i_line   + "\n" )

    #---------------------
    def write_footer(self, footer_info ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        i_line    =  BREAK_LINE + " eof footer ============"
        self.fileout.write( i_line   + "\n" )

        self.fileout.close( )

#====================================
class SelectLogWriter( object ):
    """
    this writes output in compact form to the logger
    fix first
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        all have the same init signature, may allow nulls in some cases, see code
        this guy does not need file_name
        """
        self.file_name       = file_name
        self.table_info      = table_info
        # more def in other functions

    #----------- init -----------
    def write_header(self,  ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        lines                       = []

        # will use in other methods
        self.col_names              =  [ i_format[0] for i_format in self.table_info.format_list ]
        msg   = f"write_header()  self.col_names  {self.col_names}"
        AppGlobal.print_debug( msg )

        i_line    = f"#---------- SelectLogWriter output from {AppGlobal.controller.app_name} {AppGlobal.controller.app_version}"
        lines.append( i_line  )

        lines.append( f" self.table_info.sql = {self.table_info.sql}"  )

        i_line    = f"use_table:{self.table_info.table_name}"
        lines.append( i_line  )

        msg       = "\n".join( lines )
        AppGlobal.logger.log( AppGlobal.force_log_level, msg )

    #----------------------
    def write_row(self, row_object ):
        """
        could have a write row_col function to eliminate the row object  -- but might forget column name unless tricky
        """
        msg   =  str( row_object )   #.ix_db_value
        AppGlobal.logger.log( AppGlobal.force_log_level, msg )

    #---------------------
    def write_footer(self, footer_info ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        AppGlobal.logger.log( AppGlobal.force_log_level, footer_info )

#        i_line    =  ":======== eof footer ============"
##        self.fileout.write( i_line   + "\n" )
#
##        msg       = "\n".join( lines )
#        AppGlobal.logger.log( AppGlobal.force_log_level, i_line )

#====================================
class SelectCSVWriter( object ):
    """
    Purpose: write a tab seperated file
    ?? use format column lengths
    for now make this writer out lines in order -- ?? what order
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        see class doc
        """
        self.file_name       = file_name
        self.table_info      = table_info
        # more def in other functions

    #----------- init -----------
    def write_header(self,   ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        msg   = "write header...."
        AppGlobal.print_debug( msg )

        self.fileout                = open( self.file_name, "w" ) # error !!

        self.col_names              =  [ i_format[0] for i_format in self.table_info.format_list ]
        msg   = f"write_header()  self.col_names  {self.col_names}"
        AppGlobal.print_debug( msg )

        i_line   = []
        for ix_col, i_col in enumerate( self.col_names ):
            #i_data    = self.fix_null( row[ ix_col ] )

            #i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            i_line.append( f"{i_col}" )    #may need more processing for CSV
        line = "\t".join( i_line )
        self.fileout.write( line    + "\n" )

#        self.table_info             = table_info
#
#        self.fileout                = open( self.file_name, "w" ) # error !!
#        self.col_names              = list( self.table_info.table_data_dict.keys() )
#
#        i_line    = f"#---------- file output from {AppGlobal.controller.app_name} {AppGlobal.controller.app_version}"
#        self.fileout.write( i_line   + "\n" )
#
#        i_line    = f"#sql = {sql}"
#        self.fileout.write( i_line   + "\n" )
#
#        i_line    = f"purpose:export"
#        self.fileout.write( i_line   + "\n" )
#
#        i_line    = f"use_table:{table_info.table_name}"
#        self.fileout.write( i_line   + "\n" )

    #----------------------
    def write_row(self, row_object ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        i_line   = []
        for ix_col, i_col in enumerate( self.col_names ):
            #i_data    = self.fix_null( row[ ix_col ] )

            i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            i_line.append( f"{i_data}" )    #may need more processing for CSV
        line = "\t".join( i_line )
        self.fileout.write( line    + "\n" )

    #---------------------
    def write_footer(self, footer_info ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
#        i_line    =  ":======== eof footer ============"
#        self.fileout.write( i_line   + "\n" )

        self.fileout.close( )

#====================================
class SelectTableWriter( object ):

    """
    Tabular output with justification to a .txt file
    ??do simple string formatting for now later get fancier
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        all have the same init signature, may allow nulls in some cases, see code
        """
        self.file_name       = file_name
        self.table_info      = table_info

        self.ix_format_just             = self.table_info.ix_format_just
        self.ix_format_len              = self.table_info.ix_format_len

        # more def in other functions

    #----------- init -----------
    def write_header(self, ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned, file open could fail
        """
        msg   = "write header...."
        AppGlobal.print_debug( msg )

        self.fileout                = open( self.file_name, "w" ) # error !!

        # use list comp or one time thru the list by explicit index ??
        self.col_names              =  [ i_format[0]                     for i_format in self.table_info.format_list ]
        msg   = f"write_header()  self.col_names  {self.col_names}"
        AppGlobal.print_debug( msg )

        # we are doing this 2 ways, which ??
        self.col_just               = [ i_format[ self.ix_format_just ]   for i_format in self.table_info.format_list ]
        self.col_width              = [ i_format[ self.ix_format_len ]    for i_format in self.table_info.format_list ]

        i_line    = f"#---------- file output from {AppGlobal.controller.app_name} {AppGlobal.controller.app_version}"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"# sql = {self.table_info.sql}"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"# select built from = {self.table_info.built_select_input_file_name}"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"purpose:export"
        self.fileout.write( i_line   + "\n" )

        i_line    = f"use_table: {self.table_info.table_name}"
        self.fileout.write( i_line   + "\n" )

        i_line     =  self._format_list( self.col_names )
        self.fileout.write( i_line   + "\n" )

        total_width   = 0
        for i_width in self.col_width:
            total_width  += i_width + 1
        i_line     = total_width * "-"     # save for multiple uses
        self.fileout.write( i_line   + "\n" )

    #----------------------
    def _format_list(self, a_list ):
        """
        take a list of strings and format according to col_just and col_width
        return the line a string
        """
#        msg     = f"_format_list self.col_names>> {self.col_names}"
#        AppGlobal.print_debug( msg )

        out_columns    = []
        for ix_col, i_col in enumerate( self.col_names ):
            item       = self.table_info.format_list[ ix_col ]
            col_name   = item[ 0 ]   # should be i_col  self.col_names ???

            align      = item[ self.ix_format_just   ]
            length     = item[ self.ix_format_len    ]

            # or the second way
            align      = self.col_just[  ix_col ]
            length     = self.col_width[ ix_col ]      # slide between length and width.  width would be better

            #i_data    = self.fix_null( row[ ix_col ] )
            #i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            i_data     = a_list[ ix_col ]    # ROWID comes back as an int

            if isinstance( i_data, int):
                i_data = str( i_data )
#            msg     = f"_format_list i_data>>{i_data} i_data>>{i_data}<<length>>{length}<<"
#            AppGlobal.print_debug( msg )

            i_data         = i_data[0:length ]
            data_length    = len( i_data )

            if data_length < length:   # need allignment
                if   align  == "c":
                    i_data  =  ( int( ( length - data_length )/2 ) * " " ) + i_data  + ( length * " " )
                    i_data    = i_data[0:length ]

                elif align  == "r":
                    i_data  =  ( ( length - data_length ) * " " ) + i_data

                else: # assume l
                    i_data  = i_data + ( ( length - data_length ) * " " )

            out_columns.append( i_data )
        i_line   = " | ".join( out_columns )
        return i_line

    #----------------------
    def write_row(self, row_object ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        data_columns    = []
        for ix_col, i_col in enumerate( self.col_names ):
            # list comp ?? perhaps not, or perhaps
            i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            data_columns.append( i_data )

        line    = self._format_list( data_columns )
        self.fileout.write( line   + "\n" )

    #---------------------
    def write_footer(self, footer_info ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        i_line    =  ":======== eof footer ============"
        self.fileout.write( i_line   + "\n" )

        self.fileout.close( )

#====================================
class SelectHTMLWriter( object ):
    """
    HTML.py - a Python module to easily generate HTML tables and lists | Decalage
        *>url  https://www.decalage.info/python/html
    ?? first implementation lots of room for improve  !! right now seems not to work at all
    !! add paging option
    ?? use tuple to reduce mem use
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        see calss doc
        """
        self.file_name       = file_name
        self.table_info      = table_info
        self.html_table      = None   # create in write header
        # more def in other functions
        self.fileout         = None   # create in write header  why not here

    #----------- init -----------
    def write_header(self,  ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        lines                       = []

        self.col_names              = [ i_format[0] for i_format in self.table_info.format_list ]
        self.html_table             = html.Table( header_row = self.col_names )

        msg   = f"write_header()  self.col_names  {self.col_names}"
        AppGlobal.print_debug( msg )

        self.fileout                = open( self.file_name, "w" )

        #<h1>Bob fell over the chicken. [H1]</h1>
        i_line    = f"<h2>SelectHTMLWriter output from {AppGlobal.controller.app_name} {AppGlobal.controller.app_version}</h2>"
        self.fileout.write( i_line )

        i_line    = f"<h3>SQL used was = {self.table_info.sql}</h3>"
        self.fileout.write( i_line  )

#        lines.append( i_line  )
#        lines.append( f" self.table_info.sql = {self.table_info.sql}"  )
#
#        msg       = "\n".join( lines )
#        AppGlobal.logger.log( AppGlobal.force_log_level, msg )

    #----------------------
    def write_row(self, row_object ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        for now just accumulate, then render in footer
        might want to output in pages... chunks
        """
        #row_for_html         =  [ i_column_in_row[ row_object.ix_db_value ] for i_column_in_row in row_object.edit_dict ]
        data_columns    = []
        for ix_col, i_col in enumerate( self.col_names ):
            # list comp ?? perhaps not, or perhaps
            i_data    = row_object.get_value( i_col,  row_object.ix_db_value )
            data_columns.append( i_data )

        row_for_html    =  data_columns
#        msg   = f"row_for_html >>{row_for_html}<<"
#        AppGlobal.print_debug( msg )

        self.html_table.rows.append( row_for_html )

#        htmlcode = str(t)
#        print htmlcode
#        Rows may be any iterable (list, tuple, ...) including a generator. This is useful to save memory when generating a large table.

#        msg   =  str( row_object )   #.ix_db_value
#        AppGlobal.logger.log( AppGlobal.force_log_level, msg )

    #---------------------
    def write_footer(self, footer_info ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        htmlcode      = str( self.html_table )
#        AppGlobal.print_debug( htmlcode )

        self.fileout.write( htmlcode )

#        i_line    =  ":======== eof footer ============"
#        self.fileout.write( i_line   + "\n" )

        self.fileout.close( )
#        msg       = "\n".join( lines )
#        AppGlobal.logger.log( AppGlobal.force_log_level, i_line )

# --------------- Forms -----------------------
class InputGenericFormWriter( object ):
    """
    write a from for a generic table which could be used for selection of records
    a_file_writer = InputGenericFormWriter( "input_form_generic.txt")
    a_file_writer.write_file()
    """
    #----------- init -----------
    def __init__(self, file_name, table_info = None ):
        """
        all have the same init signature, may allow nulls in some cases, see code
        """
        self.file_name     =  file_name
        pass

    #----------- init -----------
    def write_file(self, ):

        self.fileout                = open( self.file_name, "w" )

        file_text    = """#---------- file input from self.finish me
# the pound sign # marks the line as a comment
# sql =  not present for input
# purpose:  for insertion to the db use insert ...... more directions here, if exported will be export
purpose:insert
# use table is the name of the table you want to use/create, change from generic to the name you want ( lower case, no spaces  ) Russ add code to check replace
use_table:generic_table_name
# next a separator
:====================
# next sample data, name of column ( lower case, no spaces ) a colon then some data
name_first:Janet
name_middle:
name_last:Jackson
date_of_birth:
# end of this record so a separator
:====================
# and so on for more data
name_first:Janet
name_middle:
name_last:Jackson
date_of_birth:
:====================
name_first:Sarah
name_middle:Jane
name_last:Jackson
date_of_birth:2001/10/06
:====================
name_first:Mary
name_middle:Jane
name_last:Jackson
date_of_birth:2001/10/08
# you should have a line marking the end of the data
:====================
"""
        msg   = "InputGenericFormWriter.write_file...."
        AppGlobal.print_debug( msg )
        self.fileout.write( file_text )

        #self.fileout.write( f"use_table:{self.table_info.table_name}\n" )
#        i_line    =  BREAK_LINE + " eof footer ============"
#        self.fileout.write( i_line   + "\n" )
        self.fileout.close( )

# --------------- Forms -----------------------
class SelectFormWriter( object ):
    """
    write a from which could be used for selection of records
    """
    #----------- init -----------
    def __init__(self, file_name, table_info ):
        """
        all have the same init signature, may allow nulls in some cases, see code
        """
        self.file_name       = file_name
        self.table_info      = table_info
        self.column_names    = table_info.get_column_names( include_rowid = True )
        # more def in other functions

    #----------- init -----------
    def write_file(self, ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        msg   = "write_file...."
        AppGlobal.print_debug( msg )
        self._write_header()

        self._write_columns()
        self._write_where()
        self._write_order_by()

        self._write_footer( )

    #----------- init -----------
    def _write_header(self, ):
        """
        Purpose: see title - write the header section of the form
        Args: Return: state change, output
        Raises: none planned
        """

        #msg   = f"write_header()  self.col_names  {self.col_names}"
        #AppGlobal.print_debug( msg )

        self.fileout                = open( self.file_name, "w" )


        self.fileout.write( f"# genereated blank select from\n" )

        self.fileout.write( f"use_table:{self.table_info.table_name}\n" )

        self.fileout.write( f"purpose:select\n" )

    #----------------------
    def _write_columns(self,  ):
        """
        Purpose: see title - write the column name section
        Args: Return: state change, output
        Raises: none planned
        """
        self.fileout.write( "# This section determines which of the columns will be included in the output and\n" )
        self.fileout.write( "# what order the columns will be place in.\n" )

        self.fileout.write( f"section:columns\n" )

        self.fileout.write( BREAK_LINE + "\n" )

        for ix, i_column_name in enumerate( self.column_names ):
            self.fileout.write( f"{i_column_name}:\n" )

        self.fileout.write( BREAK_LINE + "\n" )

    #----------------------
    def _write_where(self,  ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
#        self.fileout.write( ":====================\n" )

        self.fileout.write( "# This section determines which of rows will included in the output.\n" )
        self.fileout.write( f"section:where\n" )
        self.fileout.write( "# use operators and values\n" )
        self.fileout.write( "#     Example: ....\n" )

        self.fileout.write( BREAK_LINE + "\n" )

        for ix, i_column_name in enumerate( self.column_names ):
            self.fileout.write( f"{i_column_name}:\n" )

        self.fileout.write( BREAK_LINE + "\n" )

   #----------------------
    def _write_order_by( self,  ):
        """
        assume last in form
        sections put break at end assume do not need at begin
        """
        self.fileout.write( "# This section determines which order the rows will be listed in the output.\n" )
        self.fileout.write( f"section:order_by\n" )
#        self.fileout.write( "# This section determines which order will be used in the output.\n" )
        self.fileout.write( "# Use a direction indicator, either ASC or DSC ( not cap sensitive)\n" )
        self.fileout.write( "# to indicate an asending or descending sort.\n" )
        self.fileout.write( "# You can sort successively on columns, reorder the listing in this section\n" )
        self.fileout.write( "# listing them in the order you want them applied.\n" )
        self.fileout.write( "#     Example:column_name ASC\n" )
        self.fileout.write( BREAK_LINE + "\n" )

        for ix, i_column_name in enumerate( self.column_names ):
            self.fileout.write( f"{i_column_name}:\n" )
        # not included in last section
#        self.fileout.write( ":====================\n" )

    #---------------------
    def _write_footer(self, ):
        """
        Purpose: see title
        Args: Return: state change, output
        Raises: none planned
        """
        i_line    =  BREAK_LINE + " eof footer ============"
        self.fileout.write( i_line   + "\n" )

        self.fileout.close( )

# =================================================

if __name__ == "__main__":
    #----- run the full app
    import  easy_db
    app   = easy_db.App(   )

# =================== eof ==============================

