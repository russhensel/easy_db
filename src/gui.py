# -*- coding: utf-8 -*-

#   gui for easy_db.py


import tkinter as Tk
import logging
import tkinter.filedialog
from   tkinter.filedialog import askopenfilename

import tkinter.ttk
import ctypes

# ----- local
import gui_helper
from   app_global import AppGlobal

def  make_check_button( p_frame,  a_text, a_variable, a_function     ):
        """
        make a combination check box and button
        """
        b_frame = Tk.Frame( p_frame, bd = 2, relief = Tk.SUNKEN, height=2 )

        a_cb = Tk.Checkbutton( b_frame, text="",          variable = a_variable,    ) # value=0 )
        a_cb.grid( row=0,  column=0 )

        a_button = Tk.Button( b_frame , width=10, height=2, text = a_text )
        a_button.config( command = a_function  )
        a_button.grid( row=0, column= 1 )

        return b_frame

#============ class =================

class DropDownWidget( Tk.Frame, ):

    def __init__(self, parent, label_text = "Choose", direct_edit = False, width = 15 ):
        """
        Args:
               parent -- must have
               label_text = "Choose", direct_edit = False

               ??width = 10
        Args:  ??thinking about -- but do some with sets
               read only
               add new to drop down ??
               set_list  ( actually a tuple )
               color
               height
               width    of what
               default text --- no just set it
               button text
               border
               for label drop down arrangement

        """
        super( DropDownWidget, self).__init__( parent, width = 20, height=20, bg ="red")

        labelTop = Tk.Label( self, text = label_text )
        labelTop.grid(column = 0, row = 0)

        self.lister       = ["list one","list two"]   # more for tests
        self.a_string_var = Tk.StringVar()

#        self.cbox = Tk.ttk.Combobox( parent, values=["one", "two" ], ) # ok # width = 10, postcommand = self.updtcblist )
#        self.cbox = Tk.ttk.Combobox( parent, values=["one"],  width = 10, postcommand = self.updtcblist )

         #postcommand = self.updtcblist )   runs at time ddarrow is pressed i think
        self.cbox = Tk.ttk.Combobox( self, values=self.lister,  width = width, textvariable = self.a_string_var, postcommand = self.updtcblist )

        if not direct_edit:
            self.cbox.state(['readonly'])   #  can drop down but no entry

#        self.cbox.bind('<<cbox.bind ComboboxSelected>>', callback_2 ) #binding of user selection with a custom callback callback when?

        self.cbox.grid( column = 1, row=0 )
#        self.cbox.current(1)    # not if box is empty

#        self.cbox.bind("<<ComboboxSelected>>", callbackFunc )
        self.cbox.bind( "<<ComboboxSelected>>", self.selected )   # "<<ComboboxSelected>>" is a virtual event are ther others
        self.counter              = 100
        self.selected_function    = None

    # -----------------------------------------
    def selected( self, event ):
        """
        call function with  selected text ?? add index ?
        """
#        AppGlobal.print_debug( f"ddw selected {event}" )
        if  self.selected_function == None:
            return

    # -----------------------------------------
    def set_selected_function( self, a_function ):
#        msg   = "set_selected_function) "
#        AppGlobal.print_debug( msg )
        self.selected_function    = a_function
#        msg   = "set_text() done "
#        AppGlobal.print_debug( msg )

    def xtra_bcb( self, ):
        pass  # think for debug ... check and delete
        msg   = f"xtra_bcb self.counter = >>{self.counter}<<"
        print( msg, flush = True )

    # -----------------------------------------
    def updtcblist( self ):
        """
        this seems to work but of what use
        """
        self.add_to_dd( "from updtcblist" )

    # -----------------------------------------
    def add_to_dd( self, new_string  ):
        """
        Purpose: see name
        Args: change to pass in new item -- still need to fix
        """
        #!! pass back index
        # add trims for spaces !!
        # works to make sure what is entered is in the drop down, but adds dups
        # need to search or have a shadow set -- what about oreder in dd, by
        # perhaps run when a get is called
#        print( "add_to_dd", flush = True )
        current          = self.a_string_var.get(  )
#        print( self.cbox['values'] )
        current_list     = list( self.cbox['values'] )
#        print( f"current_list {current_list}")

        if current in current_list:
            pass
        else:
            current_list.append( current )
            new_list         = current_list
#            print( f"current >>{current}<< append:    new_list {new_list}")
            self.cbox['values'] = new_list

#        AppGlobal.print_debug( "add_to_dd done"   )

    def set_text( self, a_string ):
#        msg   = f"set_text( >>a_string<< ) "
#        AppGlobal.print_debug( msg )
        self.a_string_var.set( a_string )
#        msg   = "set_text() done "
#        AppGlobal.print_debug( msg )

    # -----------------------------------------
    def get_text( self, ):
        #print( "get text " )
        a_string    =  self.a_string_var.get(  )
        msg         = f"get_text done a_string >>>{a_string}<<<"
        print( msg, flush = True )
        return( a_string )

    def get_list( self, ):
        #print( "get_list" )
        ret   =  self.cbox['values']
        #print( f"get_list done ret >>>{ret}<<<", flush = True )
        return( ret )

    # -----------------------------------------
    def set_list( self, a_list ):
        #print( "set_list" )
        self.cbox['values'] = a_list
        #print( f"get_list done ret >>>{ret}<<<", flush = True )
        return

    def get_index( self, ):
#        print( "get_index" )
        a_index  = self.cboxc.current()
        #print( f"get text done a_string >>>{a_string}<<<" , flush = True )
        return a_index

    # -----------------------------------------
    def set_index( self, a_index ):
#        print( "set_index" )
        self.cbox.current( a_index )
        #print( f"get text done a_string >>>{a_string}<<<" , flush = True)
        return


#============ end class =================

class BrowseWidget( Tk.Frame ):

    def __init__(self, parent, left_button_text = "FILE: ", width = 100, filetypes = [] ):
        """
        Args:
                parent -- must have

        Args:  thinking about -- but do some with sets
               color
               height
               width    of what
               default text --- no just set it
               button text
               border
        """
#        super(BrowseWidget, self).__init__( parent, width=60, height=20, bg ="red")   # seem to need something like this
#        super(BrowseWidget, self).__init__( parent, width=60, height=20, bg ="gray")
        super(BrowseWidget, self).__init__( parent, width=60, height=20, )
        self.filename     = ""
        self.label_1      = Tk.Label( self, text=left_button_text ).grid(row=1, column=0)

        self.a_string_var = Tk.StringVar()

        self.entry_1      = Tk.Entry( self , width = width, text = "bound", textvariable = self.a_string_var )
        self.entry_1.grid( row=1, column=1 )

        self.button_1 = Tk.Button( self, text="Browse...", command = self.browse )
        self.button_1.grid( row=1, column=3 )
        self.file_change_cb   = None    # see set_file_change_cb function has no arguments ( except self, a bound function )
        self.filetypes        = filetypes

    # -----------------------------------------
    def browse( self ):
        Tk.Tk().withdraw()
        #root.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",
        #           filetypes = (("jpeg files","*.jpg"),("all files","*.*")))

        self.filename     = askopenfilename( title = self.label_1,   filetypes = self.filetypes)   # filetypes=[("Text files","*.txt")])
#        AppGlobal.print_debug( f"browse() filename = >>{self.filename}<<" )

        self.entry_1.delete( 0, Tk.END)
        self.entry_1.insert( 0, self.filename )
        if self.file_change_cb is not None:
            self.file_change_cb( )

    # -----------------------------------------
    def set_text( self, a_string ):
#        AppGlobal.print_debug( "set_text " )
        self.a_string_var.set( a_string )
#        AppGlobal.print_debug( f"set_text done >>{a_string}<<" )

    # -----------------------------------------
    def get_text( self, ):
#        AppGlobal.print_debug( "get_text " )
        a_string   =  self.a_string_var.get(  )
#        AppGlobal.print_debug( f"get_text done >>{a_string}<<" )
        return( a_string )

    # -----------------------------------------
    def set_file_change_cb( self, a_function ):
#        AppGlobal.print_debug( f"set_file_change_cb  {a_function}" )
        self.file_change_cb   =  a_function
#        AppGlobal.print_debug( "set_file_change_cb done" )

#============ class =================
class GUI( object ):

    def __init__( self, controller  ):

        AppGlobal.gui       = self
        self.controller     = controller
        self.parameters     = controller.parameters
        self.logger         = logging.getLogger( AppGlobal.logger_id + ".gui")
        self.logger.info("in class GUI init") # logger not currently used by here

        # ----- start building gui
        self.root           = Tk.Tk()

        #        print( "next set icon " + str( self.parameters.os_win ) )
        if self.parameters.os_win:
            # from qt - How to set application's taskbar icon in Windows 7 - Stack Overflow
            # https://stackoverflow.com/questions/1551605/how-to-set-applications-taskbar-icon-in-windows-7/1552105#1552105

            icon = self.parameters.icon
            if not( icon is None ):
#                print( "set icon "  + str( icon ))
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID( icon )
                self.root.iconbitmap( icon )
            else:
                print( "No icon for you!"  + str( icon ))

        self.text_in        = None    # init later

        # radio button values !! this stuff needs some clean up
        self.url_to_wiki    = 999    # used in controller  set below for radio button index
        self.comma_sep_rb   = 999
        self.undent_rb      = 999
        self.lower_rb       = 999
        self.no_ws_rb       = 999

        self.text_out       = None    # used in controller? set below
        self.browse_width   = 100     # 100 good guess
        #self.default_button = 0   # buttons not currently used at all

        self.button_var     = Tk.IntVar()
        self.button_var.set( self.controller.parameters.rb_num_on )

        # define here or in the panel with waring??

        self.cb_mulp_cmd_var   = Tk.IntVar()

        self.cb_star_cmd_var   = Tk.IntVar()
        self.cb_url_var        = Tk.IntVar()
        #self.cb_pb_cmd_var     = Tk.IntVar()
        self.cb_exe_file_var   = Tk.IntVar()
        self.cb_edit_txt_var   = Tk.IntVar()

        #self.root.title( self.controller.parameters.title )   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        self.root.title( f"{self.controller.app_name} {AppGlobal.parameters.mode} version:  {self.controller.app_version}" )

        self.root.grid_columnconfigure( 0, weight=1 )
        #self.root.grid_columnconfigure( 1, weight=1 )

        #placement = gui_helper.PlaceInGrid(  1, False )

        #-------------- make the frames

        ix_row    = -1    # we are going to place frames from top to bottom

        # ----- top application button frame
        a_frame   = self.make_app_controls_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

#        # ----- make_state_controls_frame
#        a_frame   = self.make_state_controls_frame( self.root,  )
#        ix_row   += 1
#        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- what it says
        a_frame   = self.make_action_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- what it says
        a_frame   = self.make_general_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- what it says
        a_frame   = self.make_input_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- what it says
        a_frame   = self.make_select_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- what it says
        a_frame   = self.make_form_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ------ just a frame with color
        a_frame   = Tk.Frame( self.root, width=300, height=20, bg = "blue", relief = Tk.RAISED, ) # borderwidth=1 )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

#        # ------ frame: just a frame with color
#        a_frame   = Tk.Frame( self.root, width=300, height=20, bg = "green", relief = Tk.RAISED, ) # borderwidth=1 )
#        ix_row   += 1
#        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # frames on left and right
        ix_row   += 1
        # ----- frame0
        frame0 = self._make_clip_display_( self.root )
        frame0.grid( row=ix_row, column=0, sticky=Tk.N + Tk.S + Tk.E + Tk.W )
        self.root.grid_rowconfigure(    ix_row, weight=1 )

#        # ----- frame: frame_history
#        frame_history = self._make_history_frame_( self.root )
#        print( ix_row )
#        ix_row += 1
#        frame_history.grid( row=ix_row, column=0, columnspan=2, sticky=Tk.N + Tk.S + Tk.E + Tk.W)
#        self.root.grid_rowconfigure(    ix_row, weight=1 )

        # self.cb_cmd_var.set(      self.controller.parameters.cmd_on        )
        self.cb_url_var.set(      self.controller.parameters.auto_url_on   )
        self.cb_star_cmd_var.set( self.controller.parameters.star_cmd_on     )
        self.cb_exe_file_var.set( self.controller.parameters.exe_file_on   )

        self.root.geometry( self.controller.parameters.win_geometry )
        if self.parameters.os_win:
            pass
            # icon may cause problem in linux for now only use in win
            # print "in windows setting icon"
            #self.root.iconbitmap( self.parameters.icon )

    # ------------------------------------------
    def make_app_controls_frame( self, parent_frame,  ):
        """
        this contains the button area for pretty much all of my apps
        passed a parent
        returns this frame
        """
        placement = gui_helper.PlaceInGrid(  99, False )
        a_frame = Tk.Frame( parent_frame )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Ed Log" )
        a_button.config( command = self.controller.os_open_logfile  )
        placement.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Ed Parms" )
        a_button.config( command = self.controller.os_open_parmfile  )
        placement.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Restart" )
        a_button.config( command = self.controller.restart  )
        placement.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Help" )
        a_button.config( command = self.controller.os_open_help  )
        placement.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "About" )
        a_button.config( command = AppGlobal.about  )
        placement.place( a_button )

        return a_frame

    # ------------------------------------------
    def make_action_frame( self, parent_frame,  ):
        """
        action frame is buttons essentially for testing, frame will be removed later
        """
        a_frame = Tk.Frame( parent_frame )

        placer     = gui_helper.PlaceInGrid(  99, by_rows = False )
#
#        a_button = Tk.Button( a_frame , width=10, height=2, text = "select_f_file" )
#        a_button.config( command = self.controller.tbd_bcb_wf_6  )
#        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "start edit" )
        a_button.config( command = self.controller.tbd_bcb_wf_7  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "finish_edit" )
        a_button.config( command = self.controller.tbd_bcb_wf_8  )
        placer.place( a_button )

#        a_button = Tk.Button( a_frame , width=10, height=2, text = "delete select" )
#        a_button.config( command = self.controller.tbd_bcb_wf_5  )
#        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "File Manager" ) #"button 1" )
        a_button.config( command = self.controller.os_file_explorer  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Text Editor" ) # "button 2" )
        a_button.config( command = self.controller.bcb_open_text_editor  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "button 3" )
        a_button.config( command = self.controller.tbd_bcb_3  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "button 4a" )
        a_button.config( command = self.controller.tbd_bcb_4a  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "button 4b" )
        a_button.config( command = self.controller.tbd_bcb_4b  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "button 5" )
        a_button.config( command = self.controller.tbd_bcb_5  )
        placer.place( a_button )


        return a_frame
    # ------------------------------------------
    def make_general_frame( self, parent_frame,  ):
        """

        BrowseWidget
        """
        AppGlobal.print_debug("make_general_frame")
        a_frame = Tk.Frame( parent_frame, borderwidth = 2,  relief= "solid", )    # height = 60,
        placer     = gui_helper.PlaceInGrid(  99, by_rows = False )

        a_label = Tk.Label( a_frame, text = "General Settings: ",  ) #   relief = RAISED,  )
        placer.place( a_label, delta = 3 )

        self.bw_db_name    = BrowseWidget( a_frame, left_button_text = "Database File",
                                          filetypes = [( "Database Files", ".db")],
                                          width = self.browse_width )
        # not bound to anything
        placer.place( self.bw_db_name, delta = 3   )
        self.bw_db_name.set_file_change_cb( self._new_db_file_name )

        # new db button
        a_widget = Tk.Button( a_frame , width=20, height=1, text = "NewDB" )
        a_widget.config( command = self.controller.bcb_define_db   )
        placer.place( a_widget, delta = 1 )


        placer.set_row( 2  )
        placer.set_col( 0 )

        a_label = Tk.Label( a_frame, text = "Last Output: none                     ", borderwidth = 2, relief= "solid"  ) #   relief = RAISED,  )
        placer.place( a_label, delta = 3 )
        self.last_output_file_name_label  = a_label

#        placer.set_row( 3  )
#        placer.set_col( 0 )

        a_widget = Tk.Button( a_frame , width=10, height=2, text = "Edit\nOutput" )
        a_widget.config( command = self.controller.open_last_output  )
        placer.place( a_widget, delta = 1 )

#        a_label = Tk.Label( a_frame, text = "a_label",  ) #   relief = RAISED,  )
#        placer.place( a_label, delta = 1 )
#
#        a_label = Tk.Label( a_frame, text = "b_label",  ) #   relief = RAISED,  )
#        placer.place( a_label, delta = 1 )

        AppGlobal.print_debug("end make_general_frame")
        return a_frame



    # ------------------------------------------
    def make_input_frame( self, parent_frame,  ):
        """
        BrowseWidget
        """
        #rint_debug("make_input_frame() for easy_db ")
        a_frame = Tk.Frame( parent_frame, borderwidth = 2, relief= "solid", )
        placer     = gui_helper.PlaceInGrid(  99, by_rows = False )

        a_label = Tk.Label( a_frame, text = "Input Actions: ",  ) #   relief = RAISED,  )
        placer.place( a_label, delta = 3 )

        self.bw_input_file    = BrowseWidget( a_frame, left_button_text = "Input File",
                                             filetypes = [( "Text Files", ".txt")],
                                             width = self.browse_width )
        # not bound to anything
        placer.place( self.bw_input_file  )
        #self.bw_input_file.
        self.bw_input_file.set_file_change_cb( self._new_input_file_name )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Edit\nFile" )
        a_button.config( command = self.open_input_file  )
        # placement.place( a_button )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Define\n Table" )
        a_button.config( command = self.controller.bcb_define_table  )
        # placement.place( a_button )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "check_file" )
        a_button.config( command = self.controller.tbd_bcb_wf_2  )
        # placement.place( a_button )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Insert\nFile" )
        a_button.config( command = self.controller.bcb_insert_file  )
        placer.place( a_button )



        return a_frame

    # ------------------------------------------
    def make_select_frame( self, parent_frame,  ):
        """
        Selects, deletes
        """
        #rint_debug("make_select_frame() for easy_db ")

#        relief=
#        Border decoration. The default is FLAT. Other possible values are SUNKEN, RAISED, GROOVE, and RIDGE.

        a_frame = Tk.Frame( parent_frame, borderwidth = 2, relief= "solid", )   # borderwidth = 1 too small to see
        placer     = gui_helper.PlaceInGrid(  99, by_rows = False )

        a_label = Tk.Label( a_frame, text = "Select Actions: ",  ) #   relief = RAISED,  )
        placer.place( a_label, delta = 1 )

        self.bw_select_file_name    = BrowseWidget( a_frame, left_button_text = "Select File",
                                                    filetypes = [( "Text Files", ".txt")],
                                                    width     = self.browse_width )

        self.bw_select_file_name.set_file_change_cb( self._new_select_file_name )
        # not bound to anything
        placer.place( self.bw_select_file_name, delta = 5  )

        placer.set_row( 1  )
        placer.set_col( 0 )

        self.ddw_tables  = DropDownWidget( a_frame, label_text = "DB Tables", direct_edit = False, width = 10  )
        self.ddw_tables.set_selected_function( self.new_table_name )

        table_list       = [ "no db file" ]
        self.ddw_tables.set_list( table_list )
        placer.place( self.ddw_tables,   )

        self.ddw_format     = DropDownWidget( a_frame, label_text = "Format", direct_edit = False, width = 15 )
        format_list         = [ "table", "input", "py_log", "csv", "sql", "html"  ]   # ref:
        self.ddw_format.set_list( format_list )
        #self.ddw_format.set_text( "input" )  # seems fine or set the index
        self.ddw_format.set_text( format_list[ 0 ] )

        msg = f"set format{format_list[ 0 ]}"
        AppGlobal.print_debug( msg )

        placer.place( self.ddw_format  )

        placer.set_row( 2  )
        placer.set_col( 0 )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Edit Select \nFile" )
        a_button.config( command = self.open_select_file  )
        placer.place( a_button )



#        a_button = Tk.Button( a_frame , width=10, height=2, text = "select all" )
#        a_button.config( command = self.controller.tbd_bcb_wf_4a  )
        a_button = Tk.Button( a_frame , width=10, height=2, text = "Select All" )
        a_button.config( command = self.controller.bcb_select_all  )
        placer.place( a_button )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Run Select" )
        a_button.config( command = self.controller.bcb_select_using_file   )
        placer.place( a_button )

        placer.delta_col( 2 )

        a_button = Tk.Button( a_frame , width = 10, height=2, text = "Delete All" )
        a_button.config( command = self.controller.bcb_delete_all  )
        placer.place( a_button, delta = 2)

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Run Delete\nSelect" )
        a_button.config( command = self.controller.bcb_delete_select  )
        placer.place( a_button )

        return a_frame


   # ------------------------------------------
    def make_form_frame( self, parent_frame,  ):
        """
        widgets that help make blank forms
        input forms =? table forms
        select forms

        """
#        AppGlobal.print_debug("make_form_frame")

        a_frame = Tk.Frame( parent_frame, borderwidth = 2,  relief= "solid", )    # height = 60,
        placer     = gui_helper.PlaceInGrid(  99, by_rows = False )

        a_label = Tk.Label( a_frame, text = "Make blank forms: ",  ) #   relief = RAISED,  )
        placer.place( a_label, delta = 3 )

        a_button = Tk.Button( a_frame , width=10, height=2, text = "Make Generic\nInput" )
        a_button.config( command = self.controller.bcb_make_input_generic_form  )
        placer.place( a_button )


        a_button = Tk.Button( a_frame , width=10, height=2, text = "Make Table\nInput" )
        a_button.config( command = self.controller.bcb_make_table_form  )
        placer.place( a_button )

        # --- button
        a_button = Tk.Button( a_frame , width=10, height=2, text = "Make Select" ) #\nForm" )
        a_button.config( command = self.controller.bcb_make_select_form   )
        placer.place( a_button )

#        # --- button
#        a_widget = Tk.Button( a_frame , width=20, height=1, text = "InputFormxx" )
#        #a_widget.config( command = self.controller.bcb_define_db   )
#        placer.place( a_widget, delta = 1 )


#        placer.set_row( 2  )
#        placer.set_col( 0 )


#        a_widget = Tk.Button( a_frame , width=10, height=2, text = "SelectFormxx" )
#        a_widget.config( command = self.controller.open_last_output  )
#        placer.place( a_widget, delta = 1 )

#        a_label = Tk.Label( a_frame, text = "a_label",  ) #   relief = RAISED,  )
#        placer.place( a_label, delta = 1 )
#
#        a_label = Tk.Label( a_frame, text = "b_label",  ) #   relief = RAISED,  )
#        placer.place( a_label, delta = 1 )

#        AppGlobal.print_debug("end make_form_frame")
        return a_frame

    # ------------------------------------------
    def _make_clip_display_( self, parent_frame,  ):
        """
        make clipboard display area
        """
        frame0 = Tk.Frame( parent_frame )   # frame is not really needed laft over

        frame0.grid_rowconfigure(    0, weight=1 )
        frame0.grid_columnconfigure( 0, weight=1 )

        text0 = Tk.Text( frame0 , width=50, height=10 )
        #text0.takefocus = 0
        #text0.config(state=Tk.DISABLED)

        s_text0 = Tk.Scrollbar( frame0  )  # LEFT left
        s_text0.grid( row=0, column=1, sticky = Tk.N + Tk.S  )
        s_text0.config( command=text0.yview )

        text0.config( yscrollcommand=s_text0.set )
        text0.grid( row=0, column=0, sticky = Tk.N + Tk.S + Tk.E + Tk.W  )

        self.text_in  = text0   # not very ellegant

#        # spacer
#        s_frame = Tk.Frame( bframe, bg ="green", height=20 ) # width=30  )
#        s_frame.grid( row=0, column=0  )

        return frame0

    # ------------------------------------------
    def _make_history_frame_(self, parent_frame, ):
        """
        ?? and add make snipets
        returns this frame, parent should paste
        """
        a_frame    = Tk.Frame( parent_frame )
        return a_frame   # comment out more or less


    # ==================== end construction ====================
    # --------------------------   construction helpers

    def _make_titled_listbox_( self, parent_frame, a_title ):
        """
        return ( famelike_thing, listbox_thing)  ?? make a class, better acces to components
        """
        a_frame      = Tk.Frame(parent_frame)
        a_listbox    = Tk.Listbox( a_frame, height=5 )
        a_listbox.grid( column=0, row=1, sticky=(Tk.N, Tk.W, Tk.E, Tk.S) )
        s = Tk.Scrollbar( a_frame, orient=Tk.VERTICAL, command=a_listbox.yview)
        s.grid(column=1, row=1, sticky=(Tk.N, Tk.S))
        a_listbox['yscrollcommand'] = s.set
        a_label = Tk.Label( a_frame, text= a_title )
        a_label.grid( column=0, row=0, sticky=( Tk.N, Tk.E, Tk.W) )
        #  ttk.Sizegrip().grid(column=1, row=1, sticky=(Tk.S, Tk.E)) size grip not appropriate here
        a_frame.grid_columnconfigure( 0, weight=1 )
        a_frame.grid_rowconfigure(    0, weight=0 )
        a_frame.grid_rowconfigure(    1, weight=1 )
        return ( a_frame, a_listbox )

    # post init/construction helpers
    #----------------------------------------------------------------------
    def write_gui_wt(self, title, a_string ):
        """
        write to gui with a title.
        title     the title
        a_string  additional stuff to write
        make a better function with title = ""  ??
        title the string with some extra formatting
        clear and write string to input area
        """
        self.write_gui( " =============== " + title  + " ==> \n" +  a_string )   # better format or join ??

    #----------------------------------------------------------------------
    def write_gui(self, string ):
        """
        clear and write string to input area
        leave disabled
        """
#        AppGlobal.print_debug( f"write_gui: {string}")
        self.text_in['state'] = 'normal'      # normal  disabled
#        self.text_in.delete( 1.0, Tk.END )
        self.root.update()    # help single threaded ???
        self.text_in.insert( Tk.END, string +"\n" )
        self.text_in.see(    Tk.END )
        self.text_in['state'] = 'disabled'      # normal  disabled
        # self.text_in.see( Tk.END )  # add scrolling
        self.root.update()    # help single threaded ???
        # self.text_in.delete( 1.0, str( cut ) + ".0" )

    #------------------- write to gui, name should doc well enough ---------------------------------------------------
    def write_gui_db_file_name(self, string ):   # !! this is old element
        pass    # find calls and delete
#        self.label_db_file_name.config(text = f"DB: {string}     " )

    #----------------------------------------------------------------------
    def write_gui_in_file_name(self, string ):
        pass    # find calls and delete
        #self.label_in_file_name.config(text = f"Input: {string}     " )

    #----------------------------------------------------------------------
    def write_gui_out_file_name(self, string ):  # !! this is old element
        pass   # find calls and delete
#        self.label_out_file_name.config(text = f"Output: {string}   " )

    #----------------------------------------------------------------------
    def set_db_file_name(self, new_file_name ):
        """
        only thru app global
        "!! is this necessary set_db_file_name() "
        """
        self.bw_db_name.set_text( new_file_name )

    #----------------------------------------------------------------------
    def get_db_file_name(self,  ):
        """
        only thru app global
        "!! is this necessary set_db_file_name() "
        """
        return self.bw_db_name.get_text(  )

    #----------------------------------------------------------------------
    def _new_db_file_name(self, ):
        """
        use as call back from the control
        """
        new_file_name  = self.bw_db_name.get_text(  )
        msg   = f"gui._new_db_file_name() {new_file_name}"
        AppGlobal.print_debug( msg )
        self.controller.change_db_file_name( new_file_name )

    #----------------------------------------------------------------------
    def _new_input_file_name(self, ):
        """
        """
        new_file_name  = self.bw_input_file.get_text(  )
        # msg   = f"new_db_file_name() {new_file_name}"
        # print_debug( msg )
        self.controller.change_input_file_name( new_file_name )

    #----------------------------------------------------------------------
    def set_input_file_name(self, new_file_name ):
        """

        """
        self.bw_input_file.set_text( new_file_name )

    #----------------------------------------------------------------------
    def new_table_name(self, table_name ):
        """
        set might be better name ??
        """
        # msg   = f"new_db_file_name() {new_file_name}"
        # print_debug( msg )
        self.controller.change_table_name( table_name )

    #----------------------------------------------------------------------
    def open_input_file(self, ):
        """
        """
        file_name  = self.bw_input_file.get_text(  )    # get name from app global
        # msg   = f"new_db_file_name() {new_file_name}"
        # print_debug( msg )
        self.controller.os_open_text_file( file_name )

    #----------------------------------------------------------------------
    def _new_select_file_name(self, ):
        """
        trigger normally from the gui control -- if you have the name use...
        """
        new_file_name  = self.bw_select_file_name.get_text(  )
        msg   = f"-new_select_file_name() {new_file_name}"
        AppGlobal.print_debug( msg )
        self.controller.change_select_file_name( new_file_name )

    #----------------------------------------------------------------------
    def set_select_file_name(self, new_file_name ):
        """
        "!! is this necessary set_db_file_name() "
        """
        msg   = f"set_select_file_name() {new_file_name}"
        AppGlobal.print_debug( msg )
        self.bw_select_file_name.set_text( new_file_name )

    #----------------------------------------------------------------------
    def open_select_file(self, ):
        """
        """
        file_name  = AppGlobal.select_file_name
        # !! check valid !!
        msg   = f"select file {file_name}"
        AppGlobal.print_debug( msg )
        self.controller.os_open_text_file( file_name )

# =================================================

if __name__ == "__main__":
    #----- run the full app
    import  easy_db
    app   = easy_db.App(   )

# =================== eof ============================








