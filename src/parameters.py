# -*- coding: utf-8 -*-

"""

for easy db




"""



import logging
import sys
from   app_global import AppGlobal
import os

# --------- local
import db_objects
from running_on import RunningOn


class Parameters( object ):
    """
    manages parameter values use like ini file
    a struct but with a few smarts
    """
    def __init__(self,    ):

        AppGlobal.parameters   = self
        #    ---------  os platform... ----------------
        self.default_config()

#        self.os_tweaks( )
#        self.computer_name_tweaks()

        self.running_on_tweaks()

		#    -------------------- choose mode

        #self.mushroom_db_mode()
        #self.stove_db_mode()
#        self.people_db_mode()
        #self.mushroom_1_mode()
        #self.mushroom_2_mode()
#        self.mushroom_3_mode()
        self.network_db_mode()
		#    -------------------- end choose mode

        AppGlobal.parameter_tweaks()  # including restart

    # -------
    def running_on_tweaks(self,  ):
        """
        use running on tweaks as a more sophisticated  version of os_tweaks and computer name tweaks,
        """
        self.running_on         = RunningOn
        self.running_on.gather_data()

        computer_id              =   self.running_on.computer_id

        self.os_tweaks()  # general for os, later for more specific

        if computer_id == "smithers":
            self.win_geometry       = '1450x700+20+20'      # width x height position
            self.ex_editor          =  r"D:\apps\Notepad++\notepad++.exe"
            self.db_file_name       =  "smithers_db.db"

        elif computer_id == "millhouse":
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"
            #self.ex_editor          =  r"notepad"
            #self.win_geometry   = '1300x600+20+20'
            self.db_file_name       =  "millhouse_db.db"

        elif computer_id == "theprof":
            self.win_geometry       = '1450x700+20+20'      # width x height position
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"
            self.db_file_name       =  "the_prof_db.db"

        elif computer_id == "bulldog":    # may be gone
            self.ex_editor          =  r"gedit"            # ubuntu
            self.db_file_name       =  "bulldog_db.db"

        elif computer_id == "bulldog-mint-russ":
            self.ex_editor          =  r"xed"
            self.db_file_name       =  "bulldog_db.db"

        else:
            print( f"In parameters: no special settings for computer_id {computer_id}" )
            if self.running_on.os_is_win:
                self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"
            else:
                self.ex_editor          =  r"leafpad"    # linux raspberry pi maybe


# ================= mode functions: select above

   # -------
    def network_db_mode( self, ):

        self.mode              = "network_db"
        print( f"\n ========  {self.mode} =========" )

        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "xxx.txt"
        self.select_file_name    = "where_1.txt"
        self.fileout_name        = "output_file.txt"


        self.db_file_name        = "network.db"
        #self.db_file_name        = "mushroom_define.db"  # usually for testing a define
        self.default_table_name  = "devices"
        self.table_name          = "network"

        self.default_key         = "key"            # just now for testing
        self.edit_file_name      = "edit_a_record.txt"
        #self.db_file_name        = "test_make_db.db"


    # -------
    def mushroom_db_mode( self, ):

        self.mode              = "mushroom_db"
        print( f"\n ========  {self.mode} =========" )

        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "mushroom_1.txt"
        self.select_file_name    = "where_1.txt"
        self.fileout_name        = "out_file.txt"


        self.db_file_name        = "mushroom.db"
        self.db_file_name        = "mushroom_define.db"  # usually for testing a define
        self.default_table_name  = "mush"
        self.table_name          = "mush"

        self.default_key         = "bot_name"            # just now for testing
        self.edit_file_name      = "edit_a_record.txt"
        #self.db_file_name        = "test_make_db.db"

    # -------
    def people_db_mode( self, ):

        self.mode              = "people_db"
        print( f"\n ========  {self.mode} =========" )

        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "insert_example_people.txt"
        self.select_file_name    = "select_people.txt"
        self.fileout_name        = "out_file.txt"

        self.db_file_name        = "people_database.db"
        self.db_file_name        = r"D:\Russ\0000\python00\python3\_projects\easy_db\people\people_database.db"
        # parameters.db_file_name

        self.default_table_name  = "people"
        self.table_name          = "people"

        self.default_key         = "key"            # just now for testing
        self.edit_file_name      = "edit_a_record.txt"
        #self.db_file_name        = "test_make_db.db"
        #self.data_dir            = ".\stoves"
        self.select_file_name     = r"D:\Russ\0000\python00\python3\_projects\easy_db\people\select_from_people.txt"

    # -------
    def stove_db_mode( self, ):
        """
        """
        self.mode              = "stove_db"
        print( f"\n ========  {self.mode} =========" )
        self.data_dir            = ".\stoves"
        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "stove1.txt"
        self.select_file_name    = "select_stoves.txt"
        self.fileout_name        = "out_file.txt"

        self.db_file_name        = "stove_database.db"

        self.default_table_name  = "stove"
        self.table_name          = "stove"

        self.default_key         = "model"            # just now for testing
        self.edit_file_name      = "edit_a_record.txt"
        #self.db_file_name        = "test_make_db.db"

    # -------
    def mushroom_2_mode( self, ):

        self.mode              = "mushroom_2"
        print( f"\n ========  {self.mode} =========" )

        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "observation_original_input_1.txt"
        self.fileout_name        = "mush_fields.txt"

        self.db_file_name        = "testing_observation.db"
        #self.db_file_name        = "mushroom.db"

        self.default_table_name  = "observation"    # just now for testing
        self.default_key         = "key"            # just now for testing

        self.table_name          = "mush"
        self.table_name          = "observation"


        self.edit_file_name      = "edit_a_record.txt"
        #self.pylogging_fn        = "mush.py_log"   # may not be working correctly yet
        #self.db_file_name        = "test_make_db.db"
    # -------
    def mushroom_3_mode( self, ):

        self.mode              = "mushroom_3"
        print( f"\n ========  {self.mode} =========" )

        # ------------- data
        #self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "observation_original_input_1.txt"
        #self.fileout_name        = "mush_fields.txt"

        self.db_file_name        = "testing_observation.db"
        #self.db_file_name        = "mushroom.db"

        self.default_table_name  = "observation"    # just now for testing
        self.default_key         = "key"            # just now for testing

        #self.table_name          = "mush"

        self.edit_file_name      = "edit_a_record.txt"
        #self.pylogging_fn        = "mush.py_log"   # may not be working correctly yet
        #self.db_file_name        = "test_make_db.db"

    # -------
    def russ_2_mode( self, ):
        self.mode              = "Russ_2"
        pass

    # -------
    def os_tweaks( self, ):
        """
        this is an subroutine to tweak the default settings of "default_ _mode"
        for particular operating systems
        you may need to mess with this based on your os setup
        """
        if  self.os_win:
            pass
            #self.icon               = r"./clipboard_b.ico"    #  greenhouse this has issues on rasPi
            #self.icon              = None                    #  default gui icon

        else:
            pass

    # -------
    def computer_name_tweaks( self ):
        """
        this is an sufroutine to tweak the default settings of "default_mode"
        for particular computers.  Put in settings for you computer if you wish
        these are for my computers, add what you want ( or nothing ) for your computes
        """

        if self.computername == "smithers":
            self.win_geometry       = '1250x700+20+20'      # width x height position
            self.ex_editor          =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10 smithers

        elif self.computername == "millhouse":
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"    # russ win 10 millhouse
            self.win_geometry       = '1000x500+20+20'          # width x height position
            #self.pylogging_fn       = "millhouse_clipboard.py_log"   # file name for the python logging

        elif self.computername == "theprof":
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"    # russ win 10 millhouse
            self.win_geometry       = '1800x800+20+20'          # width x height position
            #self.pylogging_fn       = "millhouse_clipboard.py_log"   # file name for the python logging

        else:
            print( f"no tweaks for {self.computername}" )

    # ------->> Subroutines:  one for each mode alpha order - except tutorial
    # -------
    def default_config( self ):
        #print( "\n ======== default_config =========" )
        self.mode               = "default_config"

        # ------------- data
        self.data_dir            = "./data_1"

        # these are initial defaults
        self.filein_name         = "mushroom.txt"
        self.filein_name         = "observation_1.txt"

        self.fileout_name        = "default_fileout_name.txt"

#        print( "\n ================="*5 )

        self.db_file_name        = "mushroom.db"
        #self.db_file_name        = "test_make_db.db"

        #filein_name         = "mushroom_4_col.txt"
        #db_file_name        = "mush_4_col.db"

#        self.fileout_name        = "mush_fields.txt"
#        self.table_name          = "mush"
        # ------------- Application

        our_os = sys.platform
#        print( "our_os is ", our_os )

        #if our_os == "linux" or our_os == "linux2"  "darwin":

        if our_os == "win32":
            self.os_win = True     # right now windows and everything else
        else:
            self.os_win = False

        self.platform   = our_os    # sometimes it matters which os

        self.computername   = ( str( os.getenv( "COMPUTERNAME" ) ) ).lower()

        self.snippets           = None       # define later
        self.snip_files         = None       # define later

        self.title              = "Easy DB Utility"   # window title  !! drop for name version

        self.icon               = r"D:\Russ\0000\SpyderP\ClipBoardStuff\clipboard_c.ico"
        self.icon               = r"clipboard_c.ico"
        self.icon               = r"CHIP.ICO"

        self.id_color           = "red"                # ?? not implementd

        self.win_geometry       = '1500x800+20+20'     # width x height position
        self.win_geometry       = '900x600+700+230'     # width x height position  x, y

        self.pylogging_fn       = "easy_db.py_log"   # file name for the python logging
        #self.pylogging_fn       = "mush.py_log"
        self.logging_level      = logging.DEBUG        # .DEBUG  .INFO   logging level DEBUG will log all catured text !
#        self.logging_level      = logging.INFO
        self.logger_id          = "edb"                # id in logging file

        # ------------- file names -------------------

        # this is the name of a program: its excutable with path inof.
        # to be used in opening an exteranl editor
        self.ex_editor         =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10

        # if we are writing scratch files to run in a shell or similar.
#        self.scratch_bat       =  r"scratch.bat"   # rel or absolute?
#        self.scratch_py        =  r"scratch.py"    # rel or absolute?
#
#        self.run_py            =  r"D:\apps\Notepad++\notepad++.exe"    # program to run *>py commands  !! not implemented
#
#        # files for text editing
#        self.text_extends = [  ".txt",  ".rsh", ".ino", ".py", ".h" , ".cpp", ".py_log", ".log", ]  # include the dot! add?? log py_log
#
#        # ----------------- junk ---------------
#
#        self.startup_sec       = 10   # expected startup time 10 for smithers

        #  example use as if self.controller.parameters.os_win:

        # ========================= buttons ======================
        #------------------------- check box ---------------

        self.cmd_on            = 1     # 1 is checked or on else 0
        self.auto_url_on       = 0
        self.star_cmd_on       = 0
        self.exe_file_on       = 0

        #------------------------- radio buttons ---------------

        self.rb_num_on          = 0      # which radio button on, number is not nice, but easy !! is working ???

        self.help_fn            = "help.txt"
        # ================== snippest ============================

#        self.snippets_sort      = True                # sort snippes on key, else in file order
#        self.snippets_fn        = "snip_1.txt"
#
#        self.snip_file_sort     = True
#        self.snip_file_fn       = "snip_files_1.txt"
#        self.snip_file_fn       = "snip_files_2.txt"
#        self.snip_file_fn       = r"D:\Russ\0000\python00\python3\_projects\clipboard\Ver3\snips_file_auto.txt"
#        # need to associate with extension -- say a dict
#        self.snip_file_command  = r"D:\apps\Notepad++\notepad++.exe"  #russwin10   !! implement
#        #self.snip_file_command  = r"D:\apps\Anaconda\Lib\idlelib\idle.bat"
#        # self.ex_editor=r"D:\apps\Notepad++\notepad++.exe"#russwin10
#
#        self._read_snippets_(   self.snippets_fn  )       # read the snippets
#        self._read_snip_files_( self.snip_file_fn )
#
#        self.max_history       = 9          # !! not implemented  -- maybe never
        #---------------------------------------------------

        self.transform         = "off"   #["","",]  !! is what
        self.max_lines         = 10         # max number of lines logging  !! remove see history
        # befor older lines are truncated
        # limits memory used  0 for unlimited
        self.default_scroll    = 1          # 1 auto scroll the recieve area, else 0  not implemnted drop ??

        self.poll_delta_t      = 200        # how often wee poll for clip changes, in ms

# ------------  Internal Subs Generally should not be modified  -----------------------
# ------------------------------------
    def make_table_info_mush( self, ):

            a_table_info                   = db_objects.TableInfo()
            a_table_info.table_name        = "mush"
            a_table_info.table_key         = "key"
            a_table_info.table_data_dict   =  {'key': 'TEXT', 'bot_name': 'TEXT', 'common_name_1': 'TEXT', 'common_name_2': 'TEXT',
                                    'cap_color': 'TEXT', 'cap_dia_min': 'TEXT', 'cap_dia_max': 'TEXT', 'cap_dia': 'TEXT',
                                    'cap_shape': 'TEXT', 'veil': 'TEXT', 'flesh_color': 'TEXT', 'foot_color': 'TEXT',
                                    'gill_color': 'TEXT', 'ring_color': 'TEXT', 'volva': 'TEXT', 'odour': 'TEXT', 'eat': 'TEXT',
                                    'found_where': 'TEXT', 'seasons': 'TEXT', 'web_link_1': 'TEXT', 'looks_like_1': 'TEXT',
                                    'looks_like_2': 'TEXT', 'stain': 'TEXT', 'cap_edge': 'TEXT', 'cap_surface': 'TEXT',
                                    'gills': 'TEXT', 'stalk': 'TEXT', 'cap': 'TEXT', 'cap_bruse': 'TEXT', 'geo': 'TEXT',
                                    'flesh': 'TEXT', 'spore_print': 'TEXT', 'aub_page': 'TEXT', 'spore': 'TEXT', 'stem': 'TEXT',
                                    'spore_color': 'TEXT', 'note': 'TEXT', 'description': 'TEXT', 'bruse': 'TEXT'}

            #a_table_info.print_info()

            AppGlobal.all_table_infos[ a_table_info.table_name ] = a_table_info
            AppGlobal.current_table   =a_table_info.table_name

    # ------------------------------------
    def make_table_info_room( self,  ):

        a_table_info                   =  db_objects.TableInfo()
        a_table_info.table_name        = "room"
        a_table_info.table_key         = "key_card"
        a_table_info.table_data_dict   =  {'key': 'TEXT', 'bot_name': 'TEXT', 'common_name_1': 'TEXT', 'common_name_2': 'TEXT',
                                   'cap_color': 'TEXT', 'cap_dia_min': 'TEXT', 'cap_dia_max': 'TEXT', 'cap_dia': 'TEXT',
                                'cap_shape': 'TEXT', 'veil': 'TEXT', 'flesh_color': 'TEXT', 'foot_color': 'TEXT',
                                'gill_color': 'TEXT', 'ring_color': 'TEXT', 'volva': 'TEXT', 'odour': 'TEXT', 'eat': 'TEXT',
                                'found_where': 'TEXT', 'seasons': 'TEXT', 'web_link_1': 'TEXT', 'looks_like_1': 'TEXT',
                                'looks_like_2': 'TEXT', 'stain': 'TEXT', 'cap_edge': 'TEXT', 'cap_surface': 'TEXT',
                                'gills': 'TEXT', 'stalk': 'TEXT', 'cap': 'TEXT', 'cap_bruse': 'TEXT', 'geo': 'TEXT',
                                'flesh': 'TEXT', 'spore_print': 'TEXT', 'aub_page': 'TEXT', 'spore': 'TEXT', 'stem': 'TEXT',
                                'spore_color': 'TEXT', 'note': 'TEXT', 'description': 'TEXT', 'bruse': 'TEXT'}


        AppGlobal.all_table_infos[ a_table_info.table_name ] = a_table_info
        AppGlobal.current_table   =a_table_info.table_name

        #AppGlobal.print_info()

    # ------------------------------------
    def make_table_info_observation( self, ):

        a_table_info                   =  db_objects.TableInfo()
        a_table_info.table_name        = "observation"
        a_table_info.table_key         = "key_card"
        a_table_info.table_data_dict   =  {'where_found': 'TEXT', 'when_found': 'TEXT', 'notes': 'TEXT', 'mushroom_id_1': 'TEXT',
                                           'cap': 'TEXT', 'cap_bruse': 'TEXT', 'cap_color': 'TEXT', 'cap_dia': 'TEXT', 'cap_dia_max': 'TEXT',
                                           'cap_dia_min': 'TEXT', 'cap_edge': 'TEXT', 'cap_shape': 'TEXT', 'cap_surface': 'TEXT',
                                           'description': 'TEXT', 'flesh': 'TEXT', 'geo': 'TEXT', 'gill_color': 'TEXT', 'gills': 'TEXT',
                                           'odour': 'TEXT', 'ring_color': 'TEXT', 'spore': 'TEXT', 'spore_color': 'TEXT',
                                           'spore_print': 'TEXT', 'stain': 'TEXT', 'stalk': 'TEXT', 'stem': 'TEXT', 'veil': 'TEXT',
                                           'volva': 'TEXT'}
        a_table_info.table_data_dict   =  {'found_where': 'TEXT', 'found_when': 'TEXT', 'found_grouped': 'TEXT', 'notes': 'TEXT',
         'mushroom_id_1': 'TEXT', 'cap': 'TEXT', 'cap_bruse': 'TEXT', 'cap_color': 'TEXT', 'cap_dia': 'TEXT',
         'cap_dia_max': 'TEXT', 'cap_dia_min': 'TEXT', 'cap_edge': 'TEXT', 'cap_shape': 'TEXT', 'cap_surface': 'TEXT',
         'description': 'TEXT', 'flesh': 'TEXT', 'geo': 'TEXT', 'gill_color': 'TEXT', 'gills': 'TEXT', 'gills_attach': 'TEXT',
         'odour': 'TEXT', 'ring': 'TEXT', 'ring_color': 'TEXT', 'spore': 'TEXT', 'spore_color': 'TEXT', 'spore_print': 'TEXT',
         'stain': 'TEXT', 'stalk': 'TEXT', 'stem': 'TEXT',
         'veil': 'TEXT', 'volva': 'TEXT', 'where_found': 'TEXT', 'stalk_height': 'TEXT'}
        data_dict = {'found_where': 'TEXT', 'found_when': 'TEXT', 'found_grouped': 'TEXT', 'type': 'TEXT', 'notes': 'TEXT',
             'mushroom_id_1': 'TEXT', 'cap': 'TEXT', 'cap_bruse': 'TEXT', 'cap_color': 'TEXT', 'cap_dia': 'TEXT',
             'cap_dia_max': 'TEXT', 'cap_dia_min': 'TEXT', 'cap_edge': 'TEXT', 'cap_shape': 'TEXT', 'cap_surface': 'TEXT',
             'description': 'TEXT', 'flesh': 'TEXT', 'geo': 'TEXT', 'gill_color': 'TEXT', 'gills': 'TEXT', 'gills_attach': 'TEXT',
             'odour': 'TEXT', 'ring': 'TEXT', 'ring_color': 'TEXT', 'spore': 'TEXT', 'spore_color': 'TEXT', 'spore_print': 'TEXT',
             'stain': 'TEXT',
             'stalk': 'TEXT', 'stem': 'TEXT', 'veil': 'TEXT', 'volva': 'TEXT', 'stalk_height': 'TEXT'}

        a_table_info.table_data_dict   = data_dict

        AppGlobal.all_table_infos[ a_table_info.table_name ] = a_table_info
        AppGlobal.current_table   =a_table_info.table_name

# =================================================

if __name__ == "__main__":
    #----- run the full app
    import  easy_db
    app   = easy_db.App(   )

# =================== eof ==============================



