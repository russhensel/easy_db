# -*- coding: utf-8 -*-
"""


messing about with mushroom

copy for typical use
from app_global import AppGlobal
self.parameters        = AppGlobal.parameters
AppGlobal.parameters    = self
jack     = AppGlobal.jack

"""

#import logging
import os.path

#import importlib
#import sys
import sys
import webbrowser
from   subprocess import Popen
from   pathlib import Path
import os
import psutil
from   tkinter import messagebox
import logging


# ----------------------------------------------

def addLoggingLevel( levelName, levelNum, methodName=None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

     How to add a custom loglevel to Python's logging facility - Stack Overflow
     *>url  https://stackoverflow.com/questions/2183233/how-to-add-a-custom-loglevel-to-pythons-logging-facility

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5

    """

    if not methodName:
        methodName = levelName.lower()

    if hasattr( logging, levelName):
       #raise AttributeError('{} already defined in logging module'.format(levelName))
       return   # assum already set up ok -- could cause error in comtaminated environment

    if hasattr(logging, methodName):
       raise AttributeError('{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
       raise AttributeError('{} already defined in logger class'.format(methodName))

    # This method was inspired by the answers to Stack Overflow post
    # http://stackoverflow.com/q/2183233/2988730, especially
    # http://stackoverflow.com/a/13638084/2988730
    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, *args, **kwargs)
    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot )




#print("are we loading AppGlobal", flush = True )

# -----------------------------------
def dict_to_string( a_dict ):
        """
        Purpose:
            see name -- may be copies in various modules
        Args:
            a_dict
        Returns:
            a string
        Raises:
            none local

        """
        keys        = list( a_dict.keys() )
        lines       = []
        # some sort of list comp would be better
        for i_key in a_dict:

            lines.append( f"{i_key} =>> {a_dict[i_key]} "  )

        ret_val = "\n".join( lines )

        return ret_val


#--------------------------------
def print_debug( msg  ):  #
    """
    very temp
    all shoudl be removed
    """
    print( msg, flush = True )
    AppGlobal.logger.debug( msg )

# ------------------------
class OSCall( object, ):
    """
    try to call os based on attempts with different commands
    """
    #------------------------
    def __init__(self, command_list    ):
        """
        this is meant to be a singleton use class level only do not instatiante
        thow a exception if instantuated ??
        """
        self.command_list        = command_list     # [   r"D:\apps\Notepad++\notepad++.exe", r"gedit", r"xed", r"leafpad"   ]   # or init from parameters or put best guess first
        self.ix_command          = -1
        self.working_command     = None

    # ----------------------------------------------

    def get_next_command( self,  ):
        """
        what it says
        None if cannot find one

        """
        self.ix_command += 1
        if         self.ix_command >= len( self.command_list ):
            ret =  None
        else:
            ret =         self.command_list[ self.ix_command ]
#        print( f"command = { self.ix_command} {ret} ", flush = True )
        return ret

    # ----------------------------------------------

    def os_call( self, cmd_arg ):
        """

        """
#        proc = Popen( [ cls.parameters.ex_editor, txt_file ] )
        while True:   # will exit when it works or run out of editors
            a_command    = self.working_command
            if  a_command is None:
                a_command  = self.get_next_command( )

            if a_command is None:   # still
#                    msg = "Run out of editors to try"
#                    cls.logger.error( msg )
                    raise RuntimeError( msg )
                    break  # think we are aread done
            try:
                if cmd_arg is None:
                    proc = Popen( [ a_command,  ] )
                else:
                    proc = Popen( [ a_command, cmd_arg ] )
                self.working_command  = a_command
                break  # do not get here if exception
            except Exception as excpt:
                pass     # this should let us loop
#                 cls.logger.error( "os_open_logfile exception trying to use >" + str( cls.parameters.ex_editor ) + "< to open file >" + str( cls.parameters.pylogging_fn ) +
#                                  "< Exception " + str( excpt ) )



# ------------------------
class AppGlobal( object ):
    """
    Provices for global access to some application values and state
    also some useful functions like logging
    Singelton, implemnted at class not instance level
    generally available to most of the application through the Controllers instance variable
    Use something like ( in controller )
    from   app_global import AppGlobal
    methods should probably go somewhere else  -- the controller, or is this ok
    AppGlobal.controller = self
    """
    force_log_level         = 99    # value to force logging, high but not for errors

    logger                  = None    # AppGlobal.logger
    logger_id               = None
    #  AppGlobal.logger.log( logging.DEBUG, msg )   AppGlobal.logger.log( AppGlobal.force_log_level, msg )

    # ------------------------

    #all_data_infos      = {}
    # change data info to a dict of dicts

    current_table        = None
    all_table_infos      = {}

    #all_data_infos[ table_name ] = data_info

    # ----------------- set later, generally externally as objects check in
    controller              = None
    parameters              = None
    #logger                  = "not none"  #None
#    logger_id               = None

    gui                     = None

    table_access_for_edit   = None   # passes args from table_access.edit_one_record_1() to _2


    # needs set up and maintenance see easy_db.change_db_file_name()
    __db_file_name          = None    # may be taking out of service

    old_db_file_name        = None
    table_name              = None
    data_dir                = None

    filein_name             = None
    select_file_name        = None
    temp_out_file_name      = "D:/Russ/0000/python00/python3/_projects/mushrooms/Ver6/data_1/temp_out_file_name.txt"
    last_output_file_name = ""
    addLoggingLevel( "Notice", force_log_level, methodName=None)

    text_editors            = [   r"D:\apps\Notepad++\notepad++.exe", r"C:\apps\Notepad++\notepad++.exe", r"D:\apps\Geany\bin\geany.exe" r"notepad",
                                  r"gedit", r"xed", r"leafpad"   ]   # ?? or init from parameters or put best guess first

    ix_text_editor          = -1
    working_editor          = None

    file_explorer           = OSCall( [   r"explorer",    ] )
    file_text_editor        = OSCall( text_editors,  )

    # ----------------------------------
    # two for set and get, note name match
#    @property    # lets us get not set
#    def db_file_name( cls ):

    #-----------------------
    @classmethod
    def get_db_file_name( cls ):
        """
        """
        ret        = cls.gui.get_db_file_name()
        print( f" return db_file_name {ret} refresh tables " )

        return ret

    #-----------------------
    @classmethod
    def set_db_file_name(  cls,  arg ):
        print( f" set cls.__db_file_name {arg} refresh tables " )
        #cls.__db_file_name   = arg  # not sure this is even needed or used
        cls.gui.set_db_file_name( arg )
        cls.controller.finish_set_db_file_name( arg )
        return

    #-----------------------
    @classmethod
    def adjust_file( cls, file_name  ):
        """
        adjust file to correct directory
        """
        #ret       =  f"{cls.data_dir}{os.path}{cls.filein_name}"   # this not working
        ret       = os.path.join( cls.data_dir, file_name )
        return ret

    #-----------------------
    @classmethod
    def print_info( cls ):
        lines     = []

        i_line    = "\n"*2
        lines.append( i_line )

        i_line    = "\n\n\n============--------Info on AppGlobal --------====================="
        lines.append( i_line )

        i_line    = f"current_table: {cls.current_table}"
        lines.append( i_line )

        lines.append( f"cls.__dir__(): {cls.__dir__(AppGlobal)}" )

        # both seem to give the same result which is a dict may want to
        # print layout in a better way
#        lines.append( f"vars(AppGlobal): {vars(AppGlobal)}" )
#        lines.append( f"vars(cls): {vars(cls)}" )
        lines.append( f"vars(cls): {dict_to_string( a_dict = vars(cls))}" )

#        i_line    = f"data_dir: {cls.data_dir}"
#        lines.append( i_line )

#        i_line    = f"all_data_infos: {cls.all_table_infos}"    # not useful
#        lines.append( i_line )
        # change to use dict_to_string
        table_info_names            = list( cls.all_table_infos.keys() )

        # some sort of list comp would be better
        for i_table_info_name in table_info_names:
            #a_value   = dict[ i_col_name ]
            i_line   = "\n"*2   # " ==== {str( a_value )}"
            lines.append( i_line )

            i_line   = f"i_table_info_name = {i_table_info_name} "   # " ==== {str( a_value )}"
            lines.append( i_line )

            i_line   = f"i_table_info = {cls.all_table_infos[ i_table_info_name ]} "   # " ==== {str( a_value )}"
            lines.append( i_line )

#        i_line    = f"__dir__ {cls.__dir__( cls )}"
#        lines.append( i_line )

#        i_line   = "template i_line
#        lines.append( i_line )

        ret   =  "\n".join( lines )
        print( ret )
#        for i_line in lines:
#            print( i_line )
#    @classmethod
#    def global_logger( cls ):
#        logger

    #------------------------
    def __init__(self,   ):
        """
        this is meant to be a singleton use class level only do not instatiante
        thow a exception if instantuated ??
        """
        y=1/0
        #self.__mandatory__( controller )  # should always be used, never ( almost? ) modified
        #self.__set_default__()            # this is not required in general but lets you ignore the setting of more advanced parameters
        pass

    #------------------------
    @classmethod
    def get_table_info( cls, a_table_name ):
        a_table_info       = cls.all_table_infos[ a_table_name ]
        cls.current_table  = a_table_name

        return a_table_info

    # ----------------------------------------------
    @classmethod
    def parameter_tweaks( cls,  ):
        """
        call if necessary at end of parameters -- may make init unnecessary
        AppGlobal.parameters needs to be populated
        """
        cls.text_editors            = [   r"D:\apps\Notepad++\notepad++.exe", r"C:\apps\Notepad++\notepad++.exe",
                                      r"gedit", r"xed", r"leafpad"   ]   # or init from parameters or put best guess first

        cls.text_editors.insert( 0,  cls.parameters.ex_editor  )
        cls.ix_text_editor          = -1
        cls.working_editor          = None

        print( f"parameter tweaks {cls.text_editors}" )


    #--------------------------------
    @classmethod
    def print_debug( cls, msg  ):  #
        """
        very temp
        all shoudl be removed
        """
        print( msg, flush = True )
        cls.logger.debug( msg )

     #--------------------------------
    @classmethod
    def write_gui( cls, msg  ):  #
        """
        to keep binding low with rest of app
        """
        cls.gui.write_gui( msg )

     #--------------------------------
    @classmethod
    def gui_write_start( cls, a_msg ):
        """
        AppGlobal.gui_write_start( msg )
        """
        a_msg      =  ">>>>> " + a_msg + " >>>>>"
        cls.write_gui( a_msg  )
        cls.print_debug( a_msg )
        return a_msg

     #--------------------------------
    @classmethod
    def gui_write_finish( cls, a_msg ):
        """

        """
        a_msg      =  "<<<<< " + a_msg + " <<<<<\n\n"
        cls.write_gui( a_msg  )
        cls.print_debug( a_msg )
        return a_msg

     #--------------------------------
    @classmethod
    def gui_write_progress( cls, a_msg ):
        """

        """
        a_msg      =  "... " + a_msg + " ..."
        cls.write_gui( a_msg  )
        cls.print_debug( a_msg )
        return a_msg

     #--------------------------------
    @classmethod
    def gui_write_error( cls, a_msg ):
        """
        .write_gui_error( msg )
        """
        a_msg      =  "Problem >> " + a_msg + " "
        cls.write_gui( a_msg  )
        cls.print_debug( a_msg )
        return a_msg

    #--------------------------------
    @classmethod
    def print_debug( cls, msg  ):  #
        """
        very temp   AppGlobal.print_debug( msg )
        all shoudl be removed ??
        !! add a level or can the print output be added as a handler
        """
        if cls.logger.getEffectiveLevel() <=  logging.DEBUG :
            print( msg, flush = True )
        cls.logger.debug( msg )


    # ----------------------------------------------
    @classmethod
    def show_process_memory( cls, call_msg = "", log_level = None, print_it = False ):
        """
        log and/or print memory usage
        """
        process      = psutil.Process(os.getpid())    #  import psutil
        mem          = process.memory_info().rss
        # convert to mega and format
        mem_mega     = mem/( 1e6 )
        msg          = f"{call_msg}process memory = {mem_mega:10,.2f} mega bytes "
        if print_it:
            print( msg )
        if not ( log_level is None ):
            cls.logger.log( log_level,  msg )
        msg           =  f"{mem_mega:10,.2f} mega bytes "
        return ( mem, msg )

    # ----------------------------------------------
    @classmethod
    def log_if_wrong_thread( cls, id, msg = "forgot to include msg", main = True ):
        """
        debugging aid
        check if called by intended thread
        main thread must be set first
        ex:   AppGlobal.log_if_wrong_thread( threading.get_ident(), msg = msg, main = True  )
        """
        on_main = ( id == cls.main_thread_id )

        if main:
            ok  = on_main
        else:
            ok = not( on_main )

        if not ok:
            msg    = f"In wrong thread = {cls.name_thread( id )}: + {msg}"
            cls.logger.log( cls.force_log_level,  msg )

    # ----------------------------------------------
    @classmethod
    def name_thread( cls, id, ):
        """
        return thread name Main/Helper
        ex call:  AppGlobal.name_thread( threading.get_ident(),  )
        """
        if  cls.main_thread_id is None:
            y= 1/0   # cheap exception when main_thread not set up

        if id == cls.main_thread_id:
            ret = f"Main"
        else:
            ret = f"Helper"

        return ret

    # ----------------------------------------------
    @classmethod
    def thread_logger( cls, id, call_msg = "", log_level = None ):
        """
        debugging aid
        log a message, identifying which thread it came from
        ex call: AppGlobal.thread_logger( threading.get_ident(), "here we are", 50  )
        """
        thread_name   = cls.name_thread( id )
        msg  = f"in {thread_name} thread>> {call_msg}"

        if not ( log_level is None ):
            cls.logger.log( log_level,  msg )

    # ----------------------------------------------
    @classmethod
    def about( cls,   ):
        """
		show about box -- might be nice to make simple to go to url ( help button )
        """
        url   =  r"http://www.opencircuits.com/SmartPlug_Help_File"
        __, mem_msg   = cls.show_process_memory( )
        msg  = f"{cls.controller.app_name}  version:{cls.controller.version} \n  by Russ Hensel\n  Memory in use {mem_msg} \n  Check <Help> or \n     {url} \n     for more info."
        messagebox.showinfo( "About", msg )

    # ----------------------------------------------
    @classmethod
    def os_open_help_file( cls, help_file ):
        """
		what it says
        see parameters for different types of files and naming that will work with this
        """
        #help_file            = self.parameters.help_file
        if help_file.startswith( "http:" ) or help_file.startswith( "https:" ):
           ret  = webbrowser.open( help_file, new=0, autoraise=True )    # popopen might also work with a url
#           print( f"help http: {help_file} returned {ret}")
           return

        a_join        = Path(Path( help_file ).parent.absolute() ).joinpath( Path( help_file ).name )
#        print( f"a_join {type( a_join )} >>{a_join}<<" )

        #if a_join.endswith( ".txt" ):
        if a_join.suffix.endswith( ".txt" ):
            cls.os_open_txt_file( str(a_join) )
            return

        file_exists   = os.path.exists( a_join )
        print( f"file {a_join} exists >>{file_exists}<<" )
        #full_path     = Path( help_file ).parent.absolute()
#        print( f"a_join {a_join}" )
        help_file     = str( a_join )

        ret = os.popen( help_file )
#        print( f"help popopen  {help_file} returned {ret}")

    # ----------------------------------------------
    @classmethod
    def os_open_txt_file( cls, txt_file ):
        """
        open a text file with system configured editor
		?? could check for validity of the editor or use try except
        """
        cls.file_text_editor.os_call( txt_file )

    # ----------------------------------------------
    @classmethod
    def os_file_explorer( cls, cmd_arg = None ):
        """
        explore file system with or without an argument
        """
        cls.file_explorer.os_call( cmd_arg )

     # ----------------- debuging ----------------
    def to_str():
        """
        debug aid, but dead
        convert some of AppGlobals contents to a string for debugging - left over from some other app
        might revive or delete
        """
        a_string   = (   "AppGlobal" +
                                  str (AppGlobal.parameter_dicts ) +
                           "\n    ---------- greenhouse ----------\n" + str( AppGlobal.parameter_dicts["greenhouse"]) )
        return a_string

    def print_me():
         sys.stdout.flush()
         print("========== AppGlobal =================")
         print( AppGlobal.to_str( ) )
         sys.stdout.flush()


# =================================================

if __name__ == "__main__":
    #----- run the full app
    import  easy_db
    app   = easy_db.App(   )

# =================== eof ==============================




# ======================== eof ======================