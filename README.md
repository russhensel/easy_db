Purpose:  Easy way to create an sql database from a structured text file.  Has a GUI but much of user interface is through text files.
Quite alpha now.


Enviroment: 
* Program should run on any OS supporting Python 3.6.  
* This should include Windows, Mac, Linux, and Raspberry Pi.  
* So far only tested on 
** Windows 
** Raspberry Pi
** More coming... ( But I do not have a mac ) 

Program Status: Alpha ( pretty rough ). 

Intended for those with some Python experience who can add the files to their Python development environment ( no install features for this code ). Some dependencies will need to be installed, probably prompted by error messages. Editing of the parameter file should be easier for those with Python experience. Users should find some useful documentation in the code, this is still a work in progress. Much code has been lifted from other projects of mine, some artifacts of the other projects remain.

http://www.opencircuits.com/Python_Easy_DataBase_Project


``` 
	My Standard Disclaimer:
		If you have more than a casual interest in this project you should contact me 
		( no_spam_please_666 at comcast.net ) and see if the repository is actually in good shape.  
		I may well have improved software and or documentation.  
		I will try to answer all questions and perhaps even clean up what already exists.	
``` 		

Would You Like to Contribute??
* Lots of room for extension and improvement.
* Bug reports -- which I will read and may fix.
	
```	
	Note for contributers 
		Fixes would be great.
		Extensions would be great.
		Re write of current code would generally be discouraged... but you are free to create a fork of your own.
		( take a look at readme_rsh.txt for some additional notes on changes... )
	
```
Guide to Repository

* Root - Introduction to application
* src  - code all you need to run it, sub directories contain example files illustrating use
* images - various graphics: screen shots, and extra icons....
* wiki_etc - pdf versions of the wiki at open circuits.

